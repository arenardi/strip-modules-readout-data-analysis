#include <iostream>
#include <stdio.h>
#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include <fstream>
#include <map>
#include <string>
#include "HistHold.h" 

using namespace std;


int run(int argc, char* argv[]){
	printf("I am run \n");
	int modules = 2;
	int verbose = 0;
	int channels = 256;
	int channel_to_show = 0;
	int chip_to_show = 0;

	string out_file_name = "out.root";
	vector<string> file_names = vector<string>({"strun108_3.root","strun108_4.root", "strun108_5.root"});
	string input_file = "strun108_1.root";

	for(int i_arg=1;i_arg<argc;i_arg++){
    		if(strcmp(argv[i_arg], "-v")==0){verbose=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-chip")==0){chip_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-chan")==0){channel_to_show=atoi(argv[i_arg+1]);i_arg++;}
        	if(strcmp(argv[i_arg], "-o")==0){out_file_name=string(argv[i_arg+1]);i_arg++;}

	}
	if(chip_to_show != 0){
	channel_to_show = chip_to_show*channels+channel_to_show;
	}


	map<string,TH1F*> h;
	map<string,TH1F*>::iterator it;
	map<string,TH2F*> h2;
	map<string,TH2F*>::iterator it2;
	map<string,TH1D*> hd;
	map<string,TH1D*>::iterator itd;
	
	printf("output stored in %s\n", out_file_name.c_str());
	
	TString fName= TString(input_file.c_str());
	TFile *data_file=TFile::Open(fName);

    	for(int stream=0;stream<2;stream++){
		for(int module=0;module<modules;module++){
			//fitted mean
			h[Form("h_mean_R0H%i_stream%i",1-module, stream)] = (TH1F*)(data_file->Get(Form("h_mean%i;%i", stream, 1+module)));
			h[Form("h_mean_R0H%i_stream%i",1-module, stream)]->SetNameTitle(Form("h_mean_R0H%i_stream%i",1-module, stream),Form("h_mean_R0H%i_stream%i",1-module, stream));
			//fitted sigma
			h[Form("h_sigma_R0H%i_stream%i",1-module, stream)] = (TH1F*)(data_file->Get(Form("h_sigma%i;%i", stream, 1+module)));
			h[Form("h_sigma_R0H%i_stream%i",1-module, stream)]->SetNameTitle(Form("h_sigma_R0H%i_stream%i",1-module, stream),Form("h_sigma_R0H%i_stream%i",1-module, stream));
			//scan histo
			h2[Form("h_scan_R0H%i_stream%i",1-module, stream)] = (TH2F*)(data_file->Get(Form("h_scan%i;%i", stream, 1+module)));
			h2[Form("h_scan_R0H%i_stream%i",1-module, stream)]->SetNameTitle(Form("h_scan_R0H%i_stream%i",1-module, stream),Form("h_scan_R0H%i_stream%i",1-module, stream));
			//y projection of scan histo

			
			TH1D *hello =h2[Form("h_scan_R0H%i_stream%i",1-module, stream)]->ProjectionY();
			hd[Form("h_scan_yproj_R0H%i_stream%i",1-module, stream)]=hello;
			hd[Form("h_scan_yproj_R0H%i_stream%i",1-module, stream)]->SetNameTitle(Form("h_scan_yproj_R0H%i_stream%i",1-module, stream),Form("h_scan_yproj_R0H%i_stream%i",1-module, stream));
		}
	}

		



//	h["h_mean_R0H0_stream0"] = (TH1F*)(data_file->Get("h_mean0;2"));
//    	h["h_mean_R0H1_stream0"] = (TH1F*)(data_file->Get("h_mean0;1"));
//    	h["h_mean_R0H0_stream1"] = (TH1F*)(data_file->Get("h_mean1;2"));
//    	h["h_mean_R0H1_stream1"] = (TH1F*)(data_file->Get("h_mean1;1"));

	TFile *fout=new TFile(out_file_name.c_str(),"RECREATE");
	

	for ( it = h.begin(); it != h.end(); it++ ){	
    		std::cout << it->first << std::endl;
		it->second->Write();
	}	
	for ( it2 = h2.begin(); it2 != h2.end(); it2++ ){	
    		std::cout << it2->first << std::endl;
		it2->second->Write();
	}	
	for ( itd = hd.begin(); itd != hd.end(); itd++ ){	
    		std::cout << itd->first << std::endl;
		itd->second->Write();
	}	




	fout->Close();
	return 0;
}





int main(int argc, char* argv[]){
	clock_t beginglobal = clock();

	run(argc, argv);

	clock_t endglobal = clock();
	double elapsed_secs = double(endglobal - beginglobal) / CLOCKS_PER_SEC;
	std::cout << "Time of computing: " << elapsed_secs << " = " << elapsed_secs/60 << " Minuten" <<std::endl;
	return 0;



}

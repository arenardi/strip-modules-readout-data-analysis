#include <iostream>
#include <stdio.h>
#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include <fstream>
#include <map>
#include <string>
#include "HistThresh.h"

using namespace std;

int RunNumber, ScanNumber;
int run(int argc, char* argv[]){
	printf("I am strun_scan.root -> ");
cin >> RunNumber >> ScanNumber;
	int modules =1; //R0
	int module =2; //R2
	int chips=7;
	int hybrids = 2; //H0 and H1
	bool verbose = false;
	int channels = 256;
	int channel_to_show = -1;
	int chip_to_show = -1;
	int module_to_show=0;
	int stream_to_show = 0; //stream 0 or 1
	int hybrid_to_show = 0; //hybrid R0H 0 or 1
	bool tenpointscan = false;
	bool threepointscan = false;
	int scan  =0; //will become 3 or 10 when scan selected
	char out_file_name[30];
	sprintf(out_file_name,"out/Thresh%i.root",RunNumber);

	int option = 1;
	for(int i_arg=1;i_arg<argc;i_arg++){
    		if(strcmp(argv[i_arg], "-chip")==0){chip_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-channel")==0){channel_to_show=atoi(argv[i_arg+1]);i_arg++;}
				if(strcmp(argv[i_arg], "-module")==0){module_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-hybrid")==0){hybrid_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-stream")==0){stream_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-v")==0){verbose=true;}
    		if(strcmp(argv[i_arg], "-3")==0){threepointscan=true;printf("3 point scan\n");scan=3;}
    		if(strcmp(argv[i_arg], "-10")==0){tenpointscan=true;printf("10 point scan\n");scan=10;}
    		if(strcmp(argv[i_arg], "-option")==0){option=atoi(argv[i_arg+1]);i_arg++;}
      //  if(strcmp(argv[i_arg], "-o")==0){out_file_name=string(argv[i_arg+1]);i_arg++;}
				if(strcmp(argv[i_arg], "-run")==1007){RunNumber=atoi(argv[i_arg+1]);i_arg++;}
				if(strcmp(argv[i_arg], "-scan")==2){ScanNumber=atoi(argv[i_arg+1]);i_arg++;}

	}
//vector<string> file_names = vector<string>({"strun%i_%i.root",RunNumber,ScanNumber});

char file_name[30];
sprintf(file_name,"Files_input/strun%i_%i.root",RunNumber, ScanNumber);
printf("%s\n",file_name );
vector<string> file_names = vector<string>({});
file_names.push_back(file_name);
	if(threepointscan || tenpointscan){
		for (int i=0; i<scan-1; i++)
		{
			//file_names = vector<string>({file_name});
			ScanNumber = ScanNumber+1;
			sprintf(file_name,"strun%i_%i.root",RunNumber,ScanNumber);
			file_names.push_back(file_name);

		}}
		//if(option==1){file_names = vector<string>({"strun434_3.root","strun434_4.root", "strun434_5.root"});}
		//if(option==1){file_names = vector<string>({"strun670_23.root","strun670_24.root", "strun670_25.root"});}
		//if(option==1){file_names = vector<string>({"strun704_11.root","strun704_12.root", "strun704_13.root"});}

		//if(option==1){file_names = vector<string>({"strun%i_%i.root","strun%i_%i.root", "strun%i_%i.root",RunNumber,ScanNumber,RunNumber,ScanNumber+1,RunNumber,ScanNumber+2});}

		//if(option==0){out_file_name = "outs/run108out.root"; file_names = vector<string>({"threepointscan/strun108_3.root","threepointscan/strun108_4.root", "threepointscan/strun108_5.root"});}
		//if(option==1){out_file_name = "outs/run94out.root"; file_names = vector<string>({"threepointscan/strun94_3.root","threepointscan/strun94_4.root", "threepointscan/strun94_5.root"});}
		//if(option==2){out_file_name = "outs/run131out.root"; file_names = vector<string>({"threepointscan/strun131_3.root","threepointscan/strun131_4.root", "threepointscan/strun131_5.root"});}
/*	if(tenpointscan){
		file_names = vector<string>({"tenpointscan/strun108_49.root","tenpointscan/strun108_50.root", "tenpointscan/strun108_51.root", "tenpointscan/strun108_52.root", "tenpointscan/strun108_53.root", "tenpointscan/strun108_54.root", "tenpointscan/strun108_55.root", "tenpointscan/strun108_56.root", "tenpointscan/strun108_57.root", "tenpointscan/strun108_58.root"});
	}
*/

	//printf("output stored in %s\n", out_file_name.c_str());
	//limits of chips
	map<int,vector<int>> lims;

		lims[1]=vector<int>({1, 128});
		lims[2]=vector<int>({128, 256});
		lims[3]=vector<int>({256, 384});
		lims[4]=vector<int>({384, 512});
		lims[5]=vector<int>({512,640});
		lims[6]=vector<int>({640,768});



	if(chip_to_show ==0 || chip_to_show > 9){printf("Error: Please enter a chip out of 1-> 9 and select Stream 0 or 1 and Hybrid R0H 0 or 1. Channel can be provided within the range (1, 256).\n");return 0;}
	if(channel_to_show == 0 || channel_to_show > 256){printf("Error: Please enter a chip out of 1-> 9 and select Stream 0 or 1 and Hybrid R0H 0 or 1. Channel can be provided within the range (1, 256).\n");return 0;}
	if(chip_to_show>0 && channel_to_show>0){
		channel_to_show = lims[chip_to_show].at(0)+channel_to_show;
		chip_to_show = -1;
	}

	vector<TFile*> files = vector<TFile*>({});
	for(int file = 0; file < file_names.size(); file++){
		string input_file = file_names.at(file);
		cout<<input_file<<endl;
		TString fName= TString(input_file.c_str());
		TFile *data_file=TFile::Open(fName);
		files.push_back(data_file);
	}

	HistThresh *hist = new HistThresh(out_file_name,lims, scan);
	hist->make(files, module, hybrids, chips, verbose);
	//hist->stupidPrint();
	if(channel_to_show>0 || chip_to_show>0){
		std::cout << "S Curve requested"<< std::endl;
		hist->GetSCurve(module, hybrid_to_show, stream_to_show, channel_to_show, chip_to_show);
	}
	if(scan==3||scan==10){
		std::cout << "Scan " << scan << " requested" << std::endl;
		hist->Scan(scan, module, hybrids, chips);
	}
	hist->writeHist();
}



int main(int argc, char* argv[]){
	clock_t beginglobal = clock();

	run(argc, argv);

	clock_t endglobal = clock();
	double elapsed_secs = double(endglobal - beginglobal) / CLOCKS_PER_SEC;
	std::cout << "Time of computing: " << elapsed_secs << " Sekunden  = " << elapsed_secs/60 << " Minuten" <<std::endl;
	return 0;



}

//HistComp.cpp
#include "HistComp.h"
#include <cmath>
#include <TMath.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TColor.h>
#include <TDirectory.h>
using namespace std;

//TDirectory *cdtof = top->mkdir("tof");
//cdtof->cd()
namespace {
	template <typename T>
	std::ostream& operator<<(std::ostream& os, const vector<T>& v) {
		os << "[";
		if (v.size() > 0)
			os << v.at(0);
		for (auto itr = v.begin() + 1; itr != v.end(); ++itr) {
			os << ", " << *itr;
		}
		os << "]";
	}

	void printParams(TF1* f) {
		std::vector<Double_t> params(f->GetNpar() );
		f->GetParameters(params.data() );
		std::cout << params << std::endl;
	}
}


int HistComp::make(vector<TFile*> input, int modules,  bool v){
	if(scan==3){charges= {105, 210, 315}; charge_vals= {0.5, 1.0, 1.5};}
        else if(scan==10){charges= {1025, 2050, 3075, 4100, 5125, 6150, 7200, 8300, 9400, 9600}; charge_vals= {0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 2.0, 3.0, 4.0, 6.0};}
	vector<TFile*> data_files = input;
	data_files_size = data_files.size();
	modules = modules;
	verbose = v;
	for(int file=0; file<data_files.size();file++){
		TFile *data_file = data_files.at(file);
		for(int sc = 0; sc<scan;sc++){ 
			int charge = charges.at(sc);
			for(int module=0;module<modules;module++){
				for(int stream=0;stream<2;stream++){
					if(scan==3||scan==10){
						ht[Form("file%i_%ipointscan_R0H%i_stream%i_allchips", file+1, scan, module, stream)]=(TGraph*)data_file->Get(Form("%ipointscan_R0H%i_stream%i_allchips;1", scan, module, stream));
						ht[Form("file%i_%ipointscan_R0H%i_stream%i_allchips", file+1, scan, module, stream)]->SetNameTitle(Form("file%i_%ipointscan_R0H%i_stream%i_allchips", file+1, scan, module,     stream),Form("file%i_%ipointscan_R0H%i_stream%i_allchips", file+1, scan, module, stream));
						for(int chip=1; chip<5;chip++){
							ht[Form("file%i_%ipointscan_R0H%i_stream%i_chip%i", file+1, scan, module, stream, chip)]=(TGraph*)data_file->Get(Form("%ipointscan_R0H%i_stream%i_chip%i;1", scan, module, stream, chip));
							ht[Form("file%i_%ipointscan_R0H%i_stream%i_chip%i", file+1, scan, module, stream, chip)]->SetNameTitle(Form("file%i_%ipointscan_R0H%i_stream%i_chip%i", file+1, scan, module, stream, chip),Form("file%i_%ipointscan_R0H%i_stream%i_allchips", file+1, scan, module, stream, chip));
						}
					}
					data_file->cd();
					data_file->cd(Form("charge_%i/means/", charge));
					//cout << ">>>>means current directory: ";
					//gDirectory->pwd();
					h[Form("file%i_charge%i_hist_mean_R0H%i_stream%i_allchips", file+1, charge, module, stream)]=(TH1F*)gDirectory->Get(Form("%i_hist_mean_R0H%i_stream%i", charge, module, stream));
					h[Form("file%i_charge%i_hist_mean_R0H%i_stream%i_allchips", file+1, charge, module, stream)]->SetNameTitle(Form("file%i_charge%i_hist_mean_R0H%i_stream%i_allchips", file+1, charge, module, stream), Form("file%i_charge%i_hist_mean_R0H%i_stream%i_allchips", file+1, charge, module, stream));

						for(int chip=1; chip<5;chip++){
							h[Form("file%i_charge%i_hist_mean_R0H%i_stream%i_chip%i", file+1, charge, module, stream, chip)]=(TH1F*)gDirectory->Get(Form("%i_hist_mean_R0H%i_stream%i_chip%i", charge, module, stream, chip));
							h[Form("file%i_charge%i_hist_mean_R0H%i_stream%i_chip%i", file+1, charge, module, stream, chip)]->SetNameTitle(Form("file%i_charge%i_hist_mean_R0H%i_stream%i_chip%i", file+1, charge, module, stream, chip), Form("file%i_charge%i_hist_mean_R0H%i_stream%i_chip%i", file+1, charge, module, stream, chip));
			
						}


						data_file->cd();
						data_file->cd(Form("charge_%i/sigmas/", charge));
						//cout << ">>>> sigmas current directory: ";
						//gDirectory->pwd();
						hsig[Form("file%i_charge%i_hist_sigma_R0H%i_stream%i_allchips", file+1, charge, module, stream)]=(TH1F*)gDirectory->Get(Form("%i_hist_sigma_R0H%i_stream%i", charge, module, stream));
						hsig[Form("file%i_charge%i_hist_sigma_R0H%i_stream%i_allchips", file+1, charge, module, stream)]->SetNameTitle(Form("file%i_charge%i_hist_sigma_R0H%i_stream%i_allchips", file+1, charge, module, stream), Form("file%i_charge%i_hist_sigma_R0H%i_stream%i_allchips", file+1, charge, module, stream));

						for(int chip=1; chip<5;chip++){
							hsig[Form("file%i_charge%i_hist_sigma_R0H%i_stream%i_chip%i", file+1, charge, module, stream, chip)]=(TH1F*)gDirectory->Get(Form("%i_hist_sigma_R0H%i_stream%i_chip%i", charge, module, stream, chip));
							hsig[Form("file%i_charge%i_hist_sigma_R0H%i_stream%i_chip%i", file+1, charge, module, stream, chip)]->SetNameTitle(Form("file%i_charge%i_hist_sigma_R0H%i_stream%i_chip%i", file+1, charge, module, stream, chip), Form("file%i_charge%i_hist_sigma_R0H%i_stream%i_chip%i", file+1, charge, module, stream, chip));
						}
						data_file->cd();
				}
			}
		}
	}
return 1;
}

void HistComp::Compare(int n){

	printf("Running the compare function\n");
	vector<int>  colors = vector<int>({kViolet+4, kSpring-5, kPink-3, kYellow-2, kBlue}); 
	for(int module =0; module<2; module++){
		for(int stream =0; stream<2; stream++){
			printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
				printf("charge %i charge val %f", charge, charge_val);
						printf("1\n");	
						canvas_map[Form("means_R0H%i_stream%i_charge%i",module, stream, charge)]=new TCanvas(Form("means_R0H%i_stream%i_charge%i", module, stream, charge),Form("means_R0H%i_stream%i_charge%i", module, stream, charge), 1);
						printf("2\n");	
						legend_map[Form("means_R0H%i_stream%i_charge%i",module, stream, charge)] = new TLegend(0.5,0.5,0.8,0.7);
						printf("3\n");	
						legend_map[Form("means_R0H%i_stream%i_charge%i",module, stream, charge)]->SetHeader(Form("Means charge %1.1f  R0H%i Stream %i", charge_val, module, stream ),"C"); 
						printf("4\n");	
					
					for(int file =0; file< data_files_size; file++){
						printf("5\n");	
						
					//TH1F *mark =(TH1F*)h[Form("file%i_charge%i_hist_mean_R0H%i_stream%i_allchips",file+1, charge, module, stream)]->Clone();
						//mark->SetLineColor(colors.at(file));
					h[Form("file%i_charge%i_hist_mean_R0H%i_stream%i_allchips",file+1, charge, module, stream)]->SetLineColor(colors.at(file));
						printf("6\n");	
						//mark->SetLineWidth(1);
						h[Form("file%i_charge%i_hist_mean_R0H%i_stream%i_allchips",file+1, charge, module, stream)]->SetLineWidth(1);
						printf("7\n");	
						//mark->Draw("SAME");			
						h[Form("file%i_charge%i_hist_mean_R0H%i_stream%i_allchips",file+1, charge, module, stream)]->Draw("SAME");			
						printf("8\n");	
				
						printf("9\n");	
						
  		      				legend_map[Form("means_R0H%i_stream%i_charge%i",module, stream, charge)]->AddEntry(h[Form("file%i_charge%i_hist_mean_R0H%i_stream%i_allchips", file+1,charge, module, stream)],"Hybrid + Sensor 16 July","l");
						printf("10\n");	
  		       				legend_map[Form("means_R0H%i_stream%i_charge%i",module, stream, charge)]->AddEntry(h[Form("file%i_charge%i_hist_mean_R0H%i_stream%i_allchips", file+1, charge, module, stream)],"Hybrid 23 May","l");
  		        			legend_map[Form("means_R0H%i_stream%i_charge%i",module, stream, charge)]->AddEntry(h[Form("file%i_charge%i_hist_mean_R0H%i_stream%i_allchips", file+1, charge, module, stream)],"Hybrid 16 May","l");
	      				} 
					legend_map[Form("means_R0H%i_stream%i_charge%i",module, stream, charge)]->Draw();
	
				
			}	
		}
	}
for(auto bitch:h){
cout<<bitch.first<<endl;}
}

void HistComp::GetSCurve(int module, int stream, int channel, int chip){
	if(channel > 0){  //only a specific channel should be displayed

	}
	else if(chip > 0){ //a whole chip should be displayed

	}
}
        
void HistComp::writeHist(){
	TDirectory *compare = fout->mkdir("compare");
	compare->cd();
	for ( auto itt:canvas_map){
                std::cout<<"canvas_map: " << itt.first << std::endl;
                //itt.second->Print();
                itt.second->Write();
        }
	fout->cd();
	TDirectory *scans = fout->mkdir("scans");
	scans->cd();
	for ( auto itt:ht){
       //         std::cout<<"ht: " << itt.first << std::endl;
                itt.second->Write();
        }
	fout->cd();
	TDirectory *means = fout->mkdir("means");
	means->cd();
	for ( auto it:h){
                std::cout<<"h: " << it.first << std::endl;
                it.second->Write();
        }
	//fout->cd();
	TDirectory *sigmas = fout->mkdir("sigmas");
	sigmas->cd();
	for ( auto it:hsig){
        //       std::cout<<"hsig: " << it.first << std::endl;
                it.second->Write();
        }
	fout->Close();
}

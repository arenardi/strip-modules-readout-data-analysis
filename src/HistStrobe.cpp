//HistStrobe.cpp
#include "HistStrobe.h"
#include <cmath>
#include <TMath.h>
#include <TGraph.h>
#include <TDirectory.h>
using namespace std;

//TDirectory *cdtof = top->mkdir("tof");
namespace {
	template <typename T>
	std::ostream& operator<<(std::ostream& os, const vector<T>& v) {
		os << "[";
		if (v.size() > 0)
			os << v.at(0);
		for (auto itr = v.begin() + 1; itr != v.end(); ++itr) {
			os << ", " << *itr;
		}
		os << "]";
	}

	void printParams(TF1* f) {
		std::vector<Double_t> params(f->GetNpar() );
		f->GetParameters(params.data() );
		std::cout << params << std::endl;
	}
}


int HistStrobe::make(vector<TFile*> input, int module, int hybrids, int chips, bool v){
	vector<TFile*> data_files = input;
	module = module;
	verbose = v;
	//vector<string> charges = vector<string>({});
	//vector<float> charges = vector<float>({});
	vector<int> charges = vector<int>({});
	printf("------------------Data files size: %i\n", data_files.size());



	for(int file=0; file<data_files.size();file++){
		printf("----------------file loop: %i ", file);
		TFile *data_file = data_files.at(file);

		ht[Form("h_optimal_values")]=new TGraph();
		ht[Form("h_optimal_values")]->SetNameTitle(Form("h_optimal_values"),Form("h_optimal_values"));
		ht[Form("h_optimal_values")]->SetLineWidth(0);
		ht[Form("h_optimal_values")]->SetMarkerSize(0);

		//h scan tsent -> info on the sent triggers
			her["h_scan_tsent"] = (TH1F*)(data_file->Get("h_scan_tsent;1"));
       	 		her["h_scan_tsent"]->SetNameTitle(Form("h_scan_tsent"),Form("h_scan_tsent"));
        		her["h_scan_tsent"]->GetXaxis()->SetTitle("Channel");
        		her["h_scan_tsent"]->GetYaxis()->SetTitleOffset(1);
//        		her["h_scan_tsent"]->Write();

			//h scan evcnt -> DAQ card 0 events decoded
			her[Form("h_scan_evcnt")] = (TH1F*)(data_file->Get("h_scan_evcnt;1"));
       	 		her[Form("h_scan_evcnt")]->SetNameTitle(Form("h_scan_evcnt"),Form("h_scan_evcnt"));
        		her[Form("h_scan_evcnt")]->GetXaxis()->SetTitle("Channel");
        		her[Form("h_scan_evcnt")]->GetYaxis()->SetTitleOffset(1);
//        		her[Form("h_scan_evcnt")]->Write();

			//h scan ercnt -> DAQ card 0 decoding errors
			her[Form("h_scan_ercnt")] = (TH1F*)(data_file->Get("h_scan_ercnt;1"));
       	 		her[Form("h_scan_ercnt")]->SetNameTitle(Form("h_scan_ercnt"),Form("h_scan_ercnt"));
        		her[Form("h_scan_ercnt")]->GetXaxis()->SetTitle("Channel");
        		her[Form("h_scan_ercnt")]->GetYaxis()->SetTitleOffset(1);
//        		her[Form("h_scan_ercnt")]->Write();

			//h scan tocnt -> timeout errors
			her[Form("h_scan_tocnt")] = (TH1F*)(data_file->Get("h_scan_tocnt;1"));
       	 		her[Form("h_scan_tocnt")]->SetNameTitle(Form("h_scan_tocnt"),Form("h_scan_tocnt"));
        		her[Form("h_scan_tocnt")]->GetXaxis()->SetTitle("Channel");
        		her[Form("h_scan_tocnt")]->GetYaxis()->SetTitleOffset(1);
//        		her[Form("h_scan_tocnt")]->Write();

			//h scan xecnt -> control errors
			her[Form("h_scan_xecnt")] = (TH1F*)(data_file->Get("h_scan_xecnt;1"));
       	 		her[Form("h_scan_xecnt")]->SetNameTitle(Form("h_scan_xecnt"),Form("h_scan_xecnt"));
        		her[Form("h_scan_xecnt")]->GetXaxis()->SetTitle("Channel");
        		her[Form("h_scan_xecnt")]->GetYaxis()->SetTitleOffset(1);
//        		her[Form("h_scan_xecnt")]->Write();

			for (int hybrid=0; hybrid <hybrids; hybrid++) {

				for(int stream=0; stream< 2; stream++){
        		h[Form("h_mean_R%iH%i_stream%i",module, hybrid, stream)] = (TH1F*)(data_file->Get(Form("h_mean%i;%i", stream, hybrid+1)));
       	 		h[Form("h_mean_R%iH%i_stream%i",module, hybrid, stream)]->SetNameTitle(Form("h_mean_R%iH%i_stream%i",module, hybrid, stream),Form("h_mean_R%iH%i_stream%i",module, hybrid, stream));
        		h[Form("h_mean_R%iH%i_stream%i",module, hybrid, stream)]->GetXaxis()->SetTitle("Channel");
        		h[Form("h_mean_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitleOffset(1);
        		h[Form("h_mean_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitle("Mean (Vt50) from S Curve fit");
//			h[Form("h_mean_R%iH%i_stream%i",module, hybrid, stream)]->Write();
			h[Form("hist_mean_R%iH%i_stream%i",module, hybrid, stream)]= new TH1F(Form("hist_mean_R%iH%i_stream%i",module, hybrid, stream),Form("hist_mean_R%iH%i_stream%i",module, hybrid, stream), 80, 20, 100);
			h[Form("hist_mean_R%iH%i_stream%i",module, hybrid, stream)]->GetXaxis()->SetTitle("Mean Vt50 from S curve fit");
			h[Form("hist_mean_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitleOffset(1);
			h[Form("hist_mean_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitle("Occurences");
			h[Form("hist_mean_R%iH%i_stream%i",module, hybrid, stream)]->SetLineWidth(2.5);
			h[Form("hist_mean_R%iH%i_stream%i",module, hybrid, stream)]->SetMarkerStyle(20);

			for(int chip = 1; chip<chips; chip++){
				h[Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]= new TH1F(Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip),Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip), 50, 20, 100);
			//	h[Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->SetNameTitle(Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip),Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip));
				h[Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->GetXaxis()->SetTitle("Mean Vt50 from S curve fit");
				h[Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->GetYaxis()->SetTitleOffset(1);
				h[Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->GetYaxis()->SetTitle("Occurences");
				h[Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->SetMarkerStyle(20);

			}

			//fitted sigma
	        	hsig[Form("h_sigma_R%iH%i_stream%i",module, hybrid, stream)] = (TH1F*)(data_file->Get(Form("h_sigma%i;%i", stream, hybrid+1)));
	        	hsig[Form("h_sigma_R%iH%i_stream%i",module, hybrid, stream)]->SetNameTitle(Form("h_sigma_R%iH%i_stream%i",module, hybrid, stream),Form("h_sigma_R%iH%i_stream%i",module, hybrid, stream));
						hsig[Form("h_sigma_R%iH%i_stream%i",module, hybrid, stream)]->GetXaxis()->SetTitle("Channel");
	        	hsig[Form("h_sigma_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitleOffset(1);
	        	hsig[Form("h_sigma_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitle("Sigma (noise) from S Curve fit");
						hsig[Form("h_sigma_R%iH%i_stream%i",module, hybrid, stream)]->SetMarkerStyle(20);

//			hsig[Form("h_sigma_R%iH%i_stream%i",module, hybrid, stream)]->Write();


			hsig[Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream)]= new TH1F(Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream),Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream), 100, 0, 5);
			hsig[Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream)]->SetNameTitle(Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream),Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream));
			hsig[Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream)]->GetXaxis()->SetTitle("Noise from S curve fit");
			hsig[Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitleOffset(1);
			hsig[Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitle("Occurences");
			hsig[Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream)]->SetMarkerStyle(20);



			///fitted sigma: actual hists
			for(int chip = 1; chip<chips; chip++){
				hsig[Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]= new TH1F(Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip),Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip), 50, 0, 5);
		//		printf("chip loop: %i ", chip);
				hsig[Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->SetNameTitle(Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip),Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip));
				hsig[Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->GetXaxis()->SetTitle("Noise from S curve fit");
				hsig[Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->GetYaxis()->SetTitleOffset(1);
				hsig[Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->GetYaxis()->SetTitle("Occurences");
				hsig[Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->SetMarkerStyle(20);

				//filling the actual hists with sigma and mean values
				for(int channel = limits[chip].at(0); channel<limits[chip].at(1);channel++){
					//printf("channel loop: %i", channel);
					float value_sigma = hsig[Form("h_sigma_R%iH%i_stream%i",module, hybrid, stream)]->GetBinContent(channel);
					float value = h[Form("h_mean_R%iH%i_stream%i",module, hybrid, stream)]->GetBinContent(channel);
					hsig[Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->Fill(value_sigma);
					h[Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->Fill(value);
					h[Form("hist_mean_R%iH%i_stream%i",module, hybrid, stream)]->Fill(value);
					hsig[Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream)]->Fill(value_sigma);
					//write histos
				}
//				hsig[Form("hist_sigma_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->Write();

//				hsig[Form("hist_mean_R%iH%i_stream%i_chip%i",module, hybrid, stream,chip)]->Write();
			}
		//	printf("here writing mean hist for charge %i module %i stream %i\n", charge, module, hybrid, stream);
//			hsig[Form("hist_mean_R%iH%i_stream%i",module, hybrid, stream)]->Write();
		//	printf("here writing sigma hist for charge %i module %i stream %i\n", charge, module, hybrid, stream);
//			hsig[Form("hist_sigma_R%iH%i_stream%i",module, hybrid, stream)]->Write();

	        	//scan histo
	        	h2[Form("h_scan_R%iH%i_stream%i",module, hybrid, stream)] = (TH2F*)(data_file->Get(Form("h_scan%i;%i", stream, hybrid+1)));
	       	 	h2[Form("h_scan_R%iH%i_stream%i",module, hybrid, stream)]->SetNameTitle(Form("h_scan_R%iH%i_stream%i",module, hybrid, stream),Form("h_scan_R%iH%i_stream%i",module, hybrid, stream));
	        	h2[Form("h_scan_R%iH%i_stream%i",module, hybrid, stream)]->GetXaxis()->SetTitle("Channel");
	        	h2[Form("h_scan_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitle("Threshold DAC Counts");
	        	h2[Form("h_scan_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitleOffset(1);
	        	h2[Form("h_scan_R%iH%i_stream%i",module, hybrid, stream)]->GetZaxis()->SetTitle("Hits");
//			h2[Form("h_scan_R%iH%i_stream%i",module, hybrid, stream)]->Write();

			//y projection of scan histo
	        	hd[Form("h_scan_yproj_R%iH%i_stream%i",module, hybrid, stream)]=h2[Form("h_scan_R%iH%i_stream%i",module, hybrid, stream)]->ProjectionY();
	        	hd[Form("h_scan_yproj_R%iH%i_stream%i",module, hybrid, stream)]->SetNameTitle(Form("h_scan_yproj_R%iH%i_stream%i",module, hybrid, stream),Form("h_scan_yproj_R%iH%i_stream%i",module, hybrid, stream));
						hd[Form("h_scan_yproj_R%iH%i_stream%i",module, hybrid, stream)]->GetXaxis()->SetTitle("Threshold DAC Counts");
	        	hd[Form("h_scan_yproj_R%iH%i_stream%i",module, hybrid, stream)]->GetXaxis()->SetTitleOffset(1);
	        	hd[Form("h_scan_yproj_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitleOffset(1);
	        	hd[Form("h_scan_yproj_R%iH%i_stream%i",module, hybrid, stream)]->GetYaxis()->SetTitle("Hits");

						fits[Form("h_scan_fit_yproj_R%iH%i_stream%i",module, hybrid, stream)] = new TF1(Form("h_scanfit__yproj_R%iH%i_stream%i",module, hybrid, stream),"[0]+[1]*((1/(1+exp(-(x-[2])/[3])))*(1/(1+exp((x-[4])/[5]))))",0,250);
						fits[Form("h_scan_fit_yproj_R%iH%i_stream%i",module, hybrid, stream)]->SetParameters(10,20000,12,1,30,1);
						if(verbose){hd[Form("h_scan_yproj_R%iH%i_stream%i",module, hybrid, stream)]->Fit(fits[Form("h_scan_fit_yproj_R%iH%i_stream%i",module, hybrid, stream)], "R");}
						else if(!verbose){hd[Form("h_scan_yproj_R%iH%i_stream%i",module, hybrid, stream)]->Fit(fits[Form("h_scan_fit_yproj_R%iH%i_stream%i",module, hybrid, stream)], "RQ");}
						fits[Form("h_scan_fit_yproj_R%iH%i_stream%i",module, hybrid, stream)]->Draw();
				//	hd[Form("h_scan_yproj_R%iH%i_stream%i",module, hybrid, stream)]->Write();


		for(int chip = 1; chip <chips ; chip++)
		{GetSCurve(module,  hybrid, stream, -1, chip);}

		} //stream
	}  //hybrids

}//file

	return 1;

}



void HistStrobe::GetSCurve(int module, int hybrid, int stream, int channel, int chip){
		printf("Displaying S Curve for module R%iH%i, stream %i, chip %i \n", module, hybrid, stream, chip);
    h2[Form("h_scan_R%iH%i_stream%i_chip%i",module,  hybrid,stream, chip)]= (TH2F*)h2[Form("h_scan_R%iH%i_stream%i",module, hybrid, stream)]->Clone();
		//making it aesthetically clear
		h2[Form("h_scan_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->SetNameTitle(Form("h_scan_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip),Form("h_scan_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip));

		h2[Form("h_scan_R%iH%i_stream%i_chip%i",module,  hybrid,stream, chip)]->GetXaxis()->SetTitle("Channel");
    h2[Form("h_scan_R%iH%i_stream%i_chip%i",module,  hybrid,stream, chip)]->GetYaxis()->SetTitle("Threshold DAC Counts");
    h2[Form("h_scan_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->GetYaxis()->SetTitleOffset(1);
    h2[Form("h_scan_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->GetZaxis()->SetTitle("Hits");
		h2[Form("h_scan_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->GetXaxis()->SetRangeUser(limits[chip].at(0), limits[chip].at(1));
    hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]=(TH1D*) h2[Form("h_scan_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->ProjectionY();

		//making it aesthetically clear
		hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->SetNameTitle(Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip),Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip));
		hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,  hybrid,stream, chip)]->GetXaxis()->SetTitle("Strobe Delay [DAC]");
    hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,  hybrid,stream, chip)]->GetXaxis()->SetTitleOffset(1);
    hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->GetYaxis()->SetTitleOffset(1);
    hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->GetYaxis()->SetTitle("Hits");



		fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)] = new TF1(Form("h_scanfit__yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip),"[0]+[1]*((1/(1+exp(-(x-[2])/[3])))*(1/(1+exp((x-[4])/[5]))))",0,250);
		fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->SetParameters(10,4000,12,1,30,1);
		if(verbose){hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->Fit(fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)], "R");}
		else if(!verbose){hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->Fit(fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)], "RQ");}
		fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->Draw();
//	hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->Write();


		//ht[Form("h_optimal_values_R%iH%i_stream%i_chip%i", module, stream, chip)]=new TGraph();
		//ht[Form("h_optimal_values_R%iH%i_stream%i_chip%i", module, stream, chip)]->SetNameTitle(Form("h_optimal_values_R%iH%i_stream%i_chip%i", module, stream, chip),Form("h_optimal_values_R%iH%i_stream%i_chip%i", module, stream, chip));
		int inflUp= fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->GetParameter(2);
		int inflDown= fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module, hybrid, stream, chip)]->GetParameter(4);
		Double_t strobe= inflUp + 0.57*(inflDown-inflUp);
/*
		int first_bin = hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,  hybrid,stream, chip)]->FindFirstBinAbove(3500,1);//(4095,1);
		int last_bin = hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,  hybrid,stream, chip)]->FindLastBinAbove(3500,1);//(4095,1);
		printf("first bin: %i, last bin: %i\n", first_bin, last_bin);
		first[Form("h_optimal_values_R%iH%i_stream%i_chip%i", module, hybrid, stream, chip)]=first_bin;
		last[Form("h_optimal_values_R%iH%i_stream%i_chip%i", module,  hybrid,stream, chip)]=last_bin;
		float optimal_value = first_bin + 0.57*(last_bin - first_bin);
		optimal[Form("h_optimal_values_R%iH%i_stream%i_chip%i", module, hybrid, stream, chip)]=optimal_value;
*/
		//ht[Form("h_optimal_values_R%iH%i_stream%i_chip%i", module, stream, chip)]->SetPoint((1-module)*18+stream*9+chip-1, module*18+stream*9+chip, optimal_value);
		if(strobe>0){
		ht[Form("h_optimal_values")]->SetPoint((hybrid)*12+stream*6+chip-1, hybrid*12+stream*6+chip, strobe);}
		ht[Form("h_optimal_values")]->SetLineWidth(0);
		ht[Form("h_optimal_values")]->SetMarkerStyle(20);
		ht[Form("h_optimal_values")]->SetMarkerSize(1);
		ht[Form("h_optimal_values")]->GetYaxis()->SetRangeUser(0,30);
		ht[Form("h_optimal_values")]->GetXaxis()-> SetTitle("chip");
		ht[Form("h_optimal_values")]->GetYaxis()-> SetTitle("Strobe delay [DAC]");

}

void HistStrobe::writeHist(){
	for ( auto itt:ht ){
            //  std::cout<<"tgraPHS COMING IN!!!!! "<< itt.first << std::endl;
                itt.second->Write();
        }

	for ( auto itextra:hextra ){
              //  std::cout<<"hextra crap: " << itextra.first << std::endl;
                itextra.second->Write();
        }
//////////above here empty anyway//////////////
	TDirectory *scan = fout->mkdir("scan");
	scan->cd();
	for ( auto it:h2){
              //  std::cout<<"h2: " << it.first << std::endl;
                it.second->Write();
        }
	for( auto itd:hd ){
            //    std::cout << "hd: "<< itd.first << std::endl;
                itd.second->Write();
        }

	TDirectory *means = fout->mkdir("means");
	means->cd();
	for ( auto it:h){
          //      std::cout<<"h: " << it.first << std::endl;
                it.second->Write();
        }
fout->cd();
	TDirectory *sigmas = fout->mkdir("sigmas");
	sigmas->cd();
	for ( auto it:hsig){
            //   std::cout<<"hsig: " << it.first << std::endl;
                it.second->Write();
        }
fout->cd();
	TDirectory *errors= fout->mkdir("errors");
	errors->cd();
	for ( auto it:her){
          //     std::cout<<"hsig: " << it.first << std::endl;
                it.second->Write();
        }
fout->cd();
fout->Close();
}

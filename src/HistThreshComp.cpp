//HistThreshComp.cpp
#include "HistThreshComp.h"
#include <cmath>
#include <TMath.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TColor.h>
#include <TDirectory.h>
using namespace std;

//TDirectory *cdtof = top->mkdir("tof");
//cdtof->cd()
namespace {
	template <typename T>
	std::ostream& operator<<(std::ostream& os, const vector<T>& v) {
		os << "[";
		if (v.size() > 0)
			os << v.at(0);
		for (auto itr = v.begin() + 1; itr != v.end(); ++itr) {
			os << ", " << *itr;
		}
		os << "]";
	}

	void printParams(TF1* f) {
		std::vector<Double_t> params(f->GetNpar() );
		f->GetParameters(params.data() );
		std::cout << params << std::endl;
	}
}

int HistThreshComp::make(vector<TFile*> input, int modules, int hybrids,  bool v){

	if(scan==3){charges= {105, 210, 315}; charge_vals= {0.5, 1.0, 1.5};}
        else if(scan==10){charges= {1025, 2050, 3075, 4100, 5125, 6150, 7200, 8300, 9400, 9600}; charge_vals= {0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 2.0, 3.0, 4.0, 6.0};}
	vector<TFile*> data_files = input;
	data_files_size = data_files.size();
	modules = modules;
	verbose = v;
	for(int file=0; file<data_files.size(); file++){
		TFile *data_file = data_files.at(file);

		for(int sc = 0; sc<scan; sc++){
			int charge = charges.at(sc);
			for(int module=0; module<modules; module++){
				for(int hybrid=0; hybrid<hybrids; hybrid++){
				for(int stream=0; stream<2; stream++){

					data_file->cd();
					data_file->cd("treptgain/");
					if(scan==3||scan==10){
						//TFile *data_file = data_files.at(file);
						ht[Form("file%i_%ipointscan_R%iH%i_stream%i_allchips", file+1, scan, module, hybrid, stream)]=(TGraph*)data_file->Get(Form("%ipointscan_R%iH%i_stream%i_allchips", scan, module, hybrid, stream));
					//	ht[Form("file%i_%ipointscan_R%iH%i_stream%i_allchips", file+1, scan, module, hybrid, stream)]->SetNameTitle(Form("file%i_%ipointscan_R%iH%i_stream%i_allchips", file+1, scan, module,stream),Form("file%i_%ipointscan_R%iH%i_stream%i_allchips", file+1, scan, module, hybrid, stream));

						for(int chip=1; chip<hybrid+9;chip++){
							ht[Form("file%i_%ipointscan_R%iH%i_stream%i_chip%i", file+1, scan, module, hybrid, stream, chip)]=(TGraph*)data_file->Get(Form("%ipointscan_R%iH%i_stream%i_chip%i", scan, module, hybrid, stream, chip));

						//	ht[Form("file%i_%ipointscan_R%iH%i_stream%i_chip%i", file+1, scan, module, hybrid, stream, chip)]->SetNameTitle(Form("file%i_%ipointscan_R%iH%i_stream%i_chip%i", file+1, scan, module, hybrid, stream, chip),Form("file%i_%ipointscan_R%iH%i_stream%i_allchips", file+1, scan, module, hybrid, stream, chip));

						}
					}

					data_file->cd();
					data_file->cd(Form("charge_%i/means/", charge));
					//cout << ">>>>means current directory: ";
					//gDirectory->pwd();
					h[Form("file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips", file+1, charge, module, hybrid, stream)]=(TH1F*)gDirectory->Get(Form("%i_hist_mean_R%iH%i_stream%i", charge, module, hybrid, stream));
					//h[Form("file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips", file+1, charge, module, hybrid, stream)]->SetNameTitle(Form("file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips", file+1, charge, module, hybrid, stream), Form("file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips", file+1, charge, module, hybrid, stream));
						for(int chip=1;chip< hybrid+9;chip++){

							h[Form("file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i", file+1, charge, module, hybrid, stream, chip)]=(TH1F*)gDirectory->Get(Form("%i_hist_mean_R%iH%i_stream%i_chip%i", charge, module, hybrid, stream, chip));
							//h[Form("file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i", file+1, charge, module, hybrid, stream, chip)]->SetNameTitle(Form("file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i", file+1, charge, module, hybrid, stream, chip), Form("file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i", file+1, charge, module, hybrid, stream, chip));

						}


						data_file->cd();
						data_file->cd(Form("charge_%i/sigmas/", charge));
						//cout << ">>>> sigmas current directory: ";
						//gDirectory->pwd();
						hsig[Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips", file+1, charge, module, hybrid, stream)]=(TH1F*)gDirectory->Get(Form("%i_hist_sigma_R%iH%i_stream%i", charge, module, hybrid, stream));
						////hsig[Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips", file+1, charge, module, hybrid, stream)]->SetNameTitle(Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips", file+1, charge, module, hybrid, stream), Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips", file+1, charge, module, hybrid, stream));

						for(int chip=1;chip< hybrid+9;chip++){
							hsig[Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1, charge, module, hybrid, stream, chip)]=(TH1F*)gDirectory->Get(Form("%i_hist_sigma_R%iH%i_stream%i_chip%i", charge, module, hybrid, stream, chip));
						//	hsig[Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1, charge, module, hybrid, stream, chip)]->SetNameTitle(Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1, charge, module, hybrid, stream, chip), Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1, charge, module, hybrid, stream, chip));
						}
						data_file->cd();
				}
			}
		}}
	}
return 1;
}

void HistThreshComp::Compare(int n, vector<string> labels, vector<TFile*> data_files, int modules, int hybrids, int chip_to_show, bool allchips){

vector<int>  colors = vector<int>({kViolet+4, kSpring-5, kPink-3, kYellow-2, kBlue});
cout<<"Compare "<<endl;
//MEANS-VT50////////////////////////compound pdfs////////////////////////////////////////////////////////
	printf("Running the compare function - VT50");
	canvas_map["VT50"]=new TCanvas("VT50", "VT50",1);
	canvas_map["VT50"]->Divide(4,scan);

	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
		//		printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map["VT50"]->cd(hybrid*2+stream+1+4*scan_val);
						//canvas_map["means"]->cd(hybrid*2+stream+1+4*scan_val)->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ));
						legend_map[Form("means_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("means_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]=(TH1F*)h[Form("file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Clone();
					//	hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetNameTitle(Form("VT50: charge%1.1f R%iH%i Stream%i", charge_val, module, hybrid, stream ),Form("VT50: charge%1.1f  R%iH%i Stream%i", charge_val, module, hybrid, stream ));
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetLineColor(colors.at(file));
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetStats(0);
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetYaxis()->SetRangeUser(0, 400);
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetXaxis()->SetRangeUser(0, 150);
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetLineWidth(1);
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Draw("SAME");
  		      				legend_map[Form("means_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips", file+1,charge, module, hybrid, stream)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("means_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->Draw();


			}
		}
	}
}



//MEANS-VT50//////////////////////individual canvases//////////////////////////////////////////////////
	printf("Running the compare function - individual VT50");
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
	//			printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map_in[Form("means_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]=new TCanvas(Form("means_R%iH%i_stream%i_charge%i", module, hybrid, stream, charge),Form("means_R%iH%i_stream%i_charge%i", module, hybrid, stream, charge), 1);
						legend_map[Form("means_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("means_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Draw("SAME");
  		      				legend_map[Form("means_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_allchips", file+1,charge, module, hybrid, stream)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("means_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->Draw();

			}
		}
	}}

//SIGMAS-output noise////////////////////////compound pdfs////////////////////////////////////////////////////////
	printf("Running the compare function - OUTPUT NOISE");
	canvas_map["output_noise"]=new TCanvas("output_noise", "output_noise",1);
	canvas_map["output_noise"]->Divide(4,scan);
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
		for(int stream =0; stream<2; stream++){

			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
	//			printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map["output_noise"]->cd(hybrid*2+stream+1+4*scan_val);
						//canvas_map["means"]->cd(hybrid*2+stream+1+4*scan_val)->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ));
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->SetHeader(Form("Sigmas charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]=(TH1F*)hsig[Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Clone();
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetNameTitle(Form("OUTPUT NOISE: charge%1.1f R%iH%i Stream%i", charge_val, module, hybrid, stream ),Form("OUTPUT NOISE: charge%1.1f  R%iH%i Stream%i", charge_val, module, hybrid, stream ));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetLineColor(colors.at(file));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetStats(0);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetYaxis()->SetRangeUser(0, 45);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetXaxis()->SetRangeUser(0, 150);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetLineWidth(1);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Draw("SAME");
  		      legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips", file+1,charge, module, hybrid, stream)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->Draw();


			}
		}
	}
}


//SIGMAS- OUTPUT NOISE//////////////////////individual canvases//////////////////////////////////////////////////
	printf("Running the compare function - individual OUTPUT NOISE");
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
	//			printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map_in[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]=new TCanvas(Form("sigmas_R%iH%i_stream%i_charge%i", module, hybrid, stream, charge),Form("sigmas_R%iH%i_stream%i_charge%i", module, hybrid, stream, charge), 1);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->SetHeader(Form("Sigmas charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Draw("SAME");
  		      				legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips", file+1,charge, module, hybrid, stream)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->Draw();

			}
		}
	}}

//SIGMAS- INPUT  noise////////////////////////compound pdfs////////////////////////////////////////////////////////
	printf("Running the compare function - INPUT NOISE");
	canvas_map["input_noise"]=new TCanvas("input_noise", "input_noise",1);
	canvas_map["input_noise"]->Divide(4,scan);
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);

				//printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map["input_noise"]->cd(hybrid*2+stream+1+4*scan_val);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->SetHeader(Form("Sigmas charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ),"C");
				//	printf("1: %i, 2: %i\n", data_files.size(), data_files_size);
					for(int file =0; file< data_files_size; file++){
						TFile *data_file = data_files.at(file);

						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]=(TH1F*)hsig[Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Clone();
					//	hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetNameTitle(Form("INPUT NOISE: charge%1.1f R%iH%i Stream%i", charge_val, module, hybrid, stream ),Form("INPUT NOISE: charge%1.1f  R%iH%i Stream%i", charge_val, module, hybrid, stream ));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetLineColor(colors.at(file));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetStats(0);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetYaxis()->SetRangeUser(0, 45);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetXaxis()->SetRangeUser(0, 150);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->SetLineWidth(1);

						data_file->cd();
						data_file->cd("treptgain/");

//						ht_extra[Form("gains_%i", file)]=(TGraph*)data_file->Get(Form("%ipointscan_fit_data_gains_allchips", scan));
						ht_extra[Form("gains_%i", file)]=(TGraph*)gDirectory->Get(Form("%ipointscan_fit_data_gains_allchips", scan));
						Double_t *gain = ht_extra[Form("gains_%i",file)]->GetY();
						float floaty_boii = gain[hybrid*2+stream];
						if(floaty_boii==0.0){floaty_boii = 1; }
						//for(int i =0; i<5;i++){cout<<"HERE THE THING IS:              "<<gain[i]<<endl;}
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Scale(1/floaty_boii);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Draw("HIST SAME C");
  		      legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips", file+1,charge, module, hybrid, stream)],labels.at(file).c_str(),"l");
	      		}
					legend_map[Form("sigmas_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->Draw();


			}
		}
	}
}
//SIGMAS- INPUT NOISE//////////////////////individual canvases//////////////////////////////////////////////////
	printf("Running the compare function - individual INPUT NOISE");
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
						canvas_map_in[Form("input_noise_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]=new TCanvas(Form("input_noise_R%iH%i_stream%i_charge%i", module, hybrid, stream, charge),Form("input_noise_R%iH%i_stream%i_charge%i", module, hybrid, stream, charge), 1);
						legend_map[Form("input_noise_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("input_noise_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->SetHeader(Form("Input Noise charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]=(TH1F*)hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Clone();

					//	ht_extra[Form("gains_%i",file)]=(TGraph*)data_file->Get(Form("%ipointscan_fit_data_gains_allchips", scan));

						Double_t *gain = ht_extra[Form("gains_%i",file)]->GetY();

	//					printf("CHECK GetY() result. For R%iH%i Stream%i: \n\t\t", module, hybrid, stream);
	//						for(int i=0; i<5; i++){
	//						printf("%i: %f\n\t\t",i, gain[i]);}

						float floaty_boi = gain[hybrid*2+stream];
						if(floaty_boi==0.0){floaty_boi = 1;}
						//printf("using the mod str thing: For R%iH%i Stream%i: %f\n", module, hybrid, stream, floaty_boi);

						hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Scale(1/floaty_boi);

						hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->Draw("HIST SAME C");
  		      				legend_map[Form("input_noise_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_allchips", file+1,charge, module, hybrid, stream)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("input_noise_R%iH%i_stream%i_charge%i",module, hybrid, stream, charge)]->Draw();


			}
		}
	}}

if(chip_to_show>0){CompareChip(n, labels, data_files, chip_to_show);}
if(allchips){for(int chip = 1; chip< 10; chip++){CompareChip(n, labels, data_files, chip);}}

}



void HistThreshComp::CompareChip(int n, vector<string> labels, vector<TFile*> data_files, int chip_to_show){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////           CHIP REQUESTED         /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		printf("--------Individual chip PDFs requested------------\n");

vector<int>  colors = vector<int>({kViolet+4, kSpring-5, kPink-3, kYellow-2, kBlue});

cout<< "ciao1"<<endl;
//MEANS-VT50////////////////////////compound pdfs chip////////////////////////////////////////////////////////
	printf("Running the compare function - VT50 for chip %i", chip_to_show);
	canvas_map[Form("VT50_chip%i", chip_to_show)]=new TCanvas( Form("VT50_chip%i", chip_to_show) ,Form("VT50_chip%i", chip_to_show), 1);
	canvas_map[Form("VT50_chip%i", chip_to_show)]->Divide(4,scan);
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){

		if(hybrid==0 && chip_to_show==9){continue;}

		for(int stream =0; stream<2; stream++){
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
						canvas_map[Form("VT50_chip%i", chip_to_show)]->cd(hybrid*2+stream+1+4*scan_val);
						legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show ),"C");
					for(int file =0; file< data_files_size; file++){
						cout<< "ciao"<<endl;

						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]=(TH1F*)h[Form("file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Clone();
						//hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetNameTitle(Form("VT50: charge%1.1f R%iH%i Stream%i", charge_val, module, hybrid, stream, chip_to_show ),Form("VT50: charge%1.1f  R%iH%i Stream%i", charge_val, module, hybrid, stream, chip_to_show ));
						cout<< "ciao"<<endl;

						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineColor(colors.at(file));
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetStats(0);
						//hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->GetYaxis()->SetRangeUser(0, 45);

						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->GetXaxis()->SetRangeUser(0, 150);
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineWidth(1);
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");

								}
					legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();


			}
		}
	}
}

//MEANS-VT50//////////////////////individual canvases chip//////////////////////////////////////////////////
	printf("Running the compare function - individual VT50 - for chip %i", chip_to_show);
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){

		if(hybrid==0 && chip_to_show==9){continue;}
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
	//			printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map_in[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]=new TCanvas(Form("means_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show),Form("means_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show), 1);
						legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      				legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();


			}
		}
	}}

//SIGMAS-output noise////////////////////////compound pdfs chip////////////////////////////////////////////////////////
	printf(Form("Running the compare function - OUTPUT NOISE for chip %i", chip_to_show));
	canvas_map[Form("output_noise_chip%i", chip_to_show)]=new TCanvas(Form("output_noise_chip%i", chip_to_show), Form("output_noise_chip%i", chip_to_show),1);
	canvas_map[Form("output_noise_chip%i", chip_to_show)]->Divide(4,scan);
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){

		if(hybrid==0 && chip_to_show==9){continue;}
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
	//			printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map[Form("output_noise_chip%i", chip_to_show)]->cd(hybrid*2+stream+1+4*scan_val);
						//canvas_map["means"]->cd(hybrid*2+stream+1+4*scan_val)->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ));
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Sigmas charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]=(TH1F*)hsig[Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Clone();
					//	hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetNameTitle(Form("OUTPUT NOISE: charge%1.1f R%iH%i Stream%i Chip%i", charge_val, module, hybrid, stream, chip_to_show ),Form("OUTPUT NOISE: charge%1.1f  R%iH%i Stream%i Chip%i", charge_val, module, hybrid, stream, chip_to_show ));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineColor(colors.at(file));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetStats(0);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetYaxis()->SetRangeUser(0, 45);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetXaxis()->SetRangeUser(0, 150);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineWidth(1);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      				legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();


			}
		}
	}}



//SIGMAS- OUTPUT NOISE//////////////////////individual canvases chip//////////////////////////////////////////////////
	printf(Form("Running the compare function - individual OUTPUT NOISE", chip_to_show));
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){

		if(hybrid==0 && chip_to_show==9){continue;}
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
	//			printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map_in[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]=new TCanvas(Form("sigmas_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show),Form("sigmas_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show), 1);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Sigmas charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      				legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();


			}
		}}
	}

//SIGMAS- INPUT  noise////////////////////////compound pdfs chip////////////////////////////////////////////////////////
	printf(Form("Running the compare function - INPUT NOISE", chip_to_show));
	printf("1\n");
	canvas_map[Form("input_noise_chip%i", chip_to_show)]=new TCanvas(Form("input_noise_chip%i", chip_to_show),Form("input_noise_chip%i", chip_to_show),1);
	canvas_map[Form("input_noise_chip%i", chip_to_show)]->Divide(4,scan);
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){

		if(hybrid==0 && chip_to_show==9){continue;}
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
				//printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map[Form("input_noise_chip%i", chip_to_show)]->cd(hybrid*2+stream+1+4*scan_val);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Sigmas charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show ),"C");
				//	printf("1: %i, 2: %i\n", data_files.size(), data_files_size);
					for(int file =0; file< data_files_size; file++){
						TFile *data_file = data_files.at(file);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]=(TH1F*)hsig[Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Clone();
					//	hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetNameTitle(Form("INPUT NOISE: charge%1.1f R%iH%i Stream%i Chip%i", charge_val, module, hybrid, stream, chip_to_show ),Form("INPUT NOISE: charge%1.1f  R%iH%i Stream%i Chip%i", charge_val, module, hybrid, stream, chip_to_show ));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineColor(colors.at(file));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetStats(0);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetYaxis()->SetRangeUser(0, 45);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetXaxis()->SetRangeUser(0, 150);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineWidth(1);
						ht_extra[Form("gains_file%i_chip%i",file, chip_to_show)]=(TGraph*)data_file->Get(Form("%ipointscan_fit_data_gains", scan));

						//ht_extra[Form("gains_file%i_chip%i",file, chip_to_show)]->Print();
						Double_t *gain = ht_extra[Form("gains_file%i_chip%i",file, chip_to_show)]->GetY();
						//float gain = 7;
						float floaty_mega_boi = gain[hybrid*8+stream*4+chip_to_show];
						if(floaty_mega_boi==0.0){ floaty_mega_boi = 1;}
//
						printf("hybrid %i stream %i chip %i (counter: %i) VAL: %f\n", hybrid, stream, chip_to_show, hybrid*18+stream*9+chip_to_show ,floaty_mega_boi );
		//				for(int i =0; i<5;i++){cout<<"HERE THE THING IS:              "<<gain[i] <<"and: "<< gain[module*8+stream*4+chip_to_show]   << " for stream and module:"<< stream << " ," <<module <<endl;}
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Scale(1/floaty_mega_boi);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Scale(1/gain);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      				legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();


			}
}

			}
		}


//SIGMAS- INPUT NOISE//////////////////////individual canvases//////////////////////////////////////////////////
	printf(Form("Running the compare function - individual INPUT NOISE", chip_to_show));
	printf("again again again 4\n");
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){

		if(hybrid==0 && chip_to_show==9){continue;}
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
						canvas_map_in[Form("input_noise_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]=new TCanvas(Form("input_noise_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show),Form("input_noise_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show), 1);
						legend_map[Form("input_noise_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("input_noise_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Input Noise charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]=(TH1F*)hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Clone();

					//	ht_extra[Form("gains_%i",file)]=(TGraph*)data_file->Get(Form("%ipointscan_fit_data_gains_allchips", scan));

						Double_t *gain = ht_extra[Form("gains_file%i_chip%i",file, chip_to_show)]->GetY();

						float floaty_mega_boi = gain[hybrid*8+stream*4+chip_to_show];
						if(floaty_mega_boi==0.0){floaty_mega_boi = 1;}
						hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Scale(1/floaty_mega_boi);
						hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      				legend_map[Form("input_noise_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("input_noise_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();


			}
		}}
	}

cout<< "ciao2"<<endl;

}





void HistThreshComp::GetSCurve(int module, int hybrid, int stream, int channel, int chip){
	if(channel > 0){  //only a specific channel should be displayed

	}
	else if(chip > 0){ //a whole chip should be displayed

	}
}

void HistThreshComp::writeHist(){
	TDirectory *compare = fout->mkdir("compare");
	compare->cd();
	for ( auto itt:canvas_map){
               // std::cout<<"canvas_map: " << itt.first << std::endl;
	//	itt.second->Print(Form("pdfs/thresh/%s.pdf", itt.first.c_str()));
                itt.second->Write();
        }
	TDirectory *canvases = fout->mkdir("canvases");
	canvases->cd();
	for ( auto itt:canvas_map_in){
               // std::cout<<"canvas_map: " << itt.first << std::endl;

		//itt.second->Print(Form("pdfs/thresh/%s.pdf", itt.first.c_str()));
                itt.second->Write();
        }
	fout->cd();
	TDirectory *scans = fout->mkdir("scans");
	scans->cd();
	for ( auto itt:ht){
       //         std::cout<<"ht: " << itt.first << std::endl;
                itt.second->Write();
        }
	fout->cd();
	TDirectory *means = fout->mkdir("means");
	means->cd();
	for ( auto it:h){
                //std::cout<<"h: " << it.first << std::endl;
                it.second->Write();
        }
	//fout->cd();
	TDirectory *sigmas = fout->mkdir("sigmas");
	sigmas->cd();
	for ( auto it:hsig){
        //       std::cout<<"hsig: " << it.first << std::endl;
                it.second->Write();
        }
	TDirectory *gains = fout->mkdir("gains");
	gains->cd();
	for ( auto itt:ht_extra){
               // std::cout<<"canvas_map: " << itt.first << std::endl;

	//	itt.second->Print(Form("pdfs/thresh/%s.pdf", itt.first.c_str()));
                itt.second->Write();
        }
	fout->Close();
}

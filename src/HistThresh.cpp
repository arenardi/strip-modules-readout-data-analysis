//HistThresh.cpp
#include "HistThresh.h"
#include <cmath>
#include <TMath.h>
#include <TGraph.h>
#include <TDirectory.h>
using namespace std;

//TDirectory *cdtof = top->mkdir("tof");
//cdtof->cd()
namespace {
	template <typename T>
	std::ostream& operator<<(std::ostream& os, const vector<T>& v) {
		os << "[";
		if (v.size() > 0)
			os << v.at(0);
		for (auto itr = v.begin() + 1; itr != v.end(); ++itr) {
			os << ", " << *itr;
		}
		os << "]";
	}

	void printParams(TF1* f) {
		std::vector<Double_t> params(f->GetNpar() );
		f->GetParameters(params.data() );
		std::cout << params << std::endl;
	}
}


int HistThresh::make(vector<TFile*> input, int module, int hybrids, int chips, bool v){
	vector<TFile*> data_files = input;
	module=module;
	//hybrids = hybrids;
	verbose = v;
	//vector<string> charges = vector<string>({});
	//vector<float> charges = vector<float>({});
	vector<int> charges = vector<int>({});
	//printf("Data files size: %i\n", data_files.size());
	if(data_files.size()==1){charges= {0};}
	else if(data_files.size()==3){charges= {105, 210, 315};}
	//else if(data_files.size()==3){charges= {0.5, 1.0, 1.5};}
	else if(data_files.size()==10){charges= {1025, 2050, 3075, 4100, 5125, 6150, 7200, 8300, 9400, 9600};}
	//else if(data_files.size()==10){charges= {0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 2.0, 3.0, 4.0, 6.0};}
	else{printf("Error: require either 1 file for testing, 3 for a 3 point scan or 10 for a 10 point scan\n"); return 0;}

	printf("check charges:\t");
	for(int i=0; i<charges.size(); i++){
		printf("%i\t", charges.at(i));
	}

	for(int file=0; file<data_files.size();file++){
		//printf("file loop: %i ", file);
		TFile *data_file = data_files.at(file);
		int charge = charges.at(file);
		//string charge = to_string(chargename);
		//charge = charge.replace(charge.find("."), 1, "p");



		TDirectory *charge_dir = fout->mkdir(Form("charge_%i",charge));
		charge_dir->cd();
		TDirectory *error_dir = charge_dir->mkdir("error_hists");
		error_dir->cd();
			//h scan tsent -> info on the sent triggers
			h[Form("%i_h_scan_tsent",charge)] = (TH1F*)(data_file->Get("h_scan_tsent;1"));
       	 		h[Form("%i_h_scan_tsent",charge)]->SetNameTitle(Form("%i_h_scan_tsent",charge),Form("%i_h_scan_tsent",charge));
        		h[Form("%i_h_scan_tsent",charge)]->GetXaxis()->SetTitle("Channel");
        		h[Form("%i_h_scan_tsent",charge)]->GetYaxis()->SetTitleOffset(1);
        		//h[Form("%i_h_scan_tsent",charge)]->GetYaxis()->SetTitle("Mean (Vt50) from S Curve fit");
        		h[Form("%i_h_scan_tsent",charge)]->Write();

			//h scan evcnt -> DAQ card 0 events decoded
			h[Form("%i_h_scan_evcnt",charge)] = (TH1F*)(data_file->Get("h_scan_evcnt;1"));
       	 		h[Form("%i_h_scan_evcnt",charge)]->SetNameTitle(Form("%i_h_scan_evcnt",charge),Form("%i_h_scan_evcnt",charge));
        		h[Form("%i_h_scan_evcnt",charge)]->GetXaxis()->SetTitle("Channel");
        		h[Form("%i_h_scan_evcnt",charge)]->GetYaxis()->SetTitleOffset(1);
        		//h[Form("%i_h_scan_evcnt",charge)]->GetYaxis()->SetTitle("Mean (Vt50) from S Curve fit");
        		h[Form("%i_h_scan_evcnt",charge)]->Write();

			//h scan ercnt -> DAQ card 0 decoding errors
			h[Form("%i_h_scan_ercnt",charge)] = (TH1F*)(data_file->Get("h_scan_ercnt;1"));
       	 		h[Form("%i_h_scan_ercnt",charge)]->SetNameTitle(Form("%i_h_scan_ercnt",charge),Form("%i_h_scan_ercnt",charge));
        		h[Form("%i_h_scan_ercnt",charge)]->GetXaxis()->SetTitle("Channel");
        		h[Form("%i_h_scan_ercnt",charge)]->GetYaxis()->SetTitleOffset(1);
        		//h[Form("%i_h_scan_ercnt",charge)]->GetYaxis()->SetTitle("Mean (Vt50) from S Curve fit");
        		h[Form("%i_h_scan_ercnt",charge)]->Write();

			//h scan tocnt -> timeout errors
			h[Form("%i_h_scan_tocnt",charge)] = (TH1F*)(data_file->Get("h_scan_tocnt;1"));
       	 		h[Form("%i_h_scan_tocnt",charge)]->SetNameTitle(Form("%i_h_scan_tocnt",charge),Form("%i_h_scan_tocnt",charge));
        		h[Form("%i_h_scan_tocnt",charge)]->GetXaxis()->SetTitle("Channel");
        		h[Form("%i_h_scan_tocnt",charge)]->GetYaxis()->SetTitleOffset(1);
        		//h[Form("%i_h_scan_tocnt",charge)]->GetYaxis()->SetTitle("Mean (Vt50) from S Curve fit");
        		h[Form("%i_h_scan_tocnt",charge)]->Write();

			//h scan xecnt -> control errors
			h[Form("%i_h_scan_xecnt",charge)] = (TH1F*)(data_file->Get("h_scan_xecnt;1"));
       	 		h[Form("%i_h_scan_xecnt",charge)]->SetNameTitle(Form("%i_h_scan_xecnt",charge),Form("%i_h_scan_xecnt",charge));
        		h[Form("%i_h_scan_xecnt",charge)]->GetXaxis()->SetTitle("Channel");
        		h[Form("%i_h_scan_xecnt",charge)]->GetYaxis()->SetTitleOffset(1);
        		//h[Form("%i_h_scan_xecnt",charge)]->GetYaxis()->SetTitle("Mean (Vt50) from S Curve fit");
        		h[Form("%i_h_scan_xecnt",charge)]->Write();


		charge_dir->cd();
		TDirectory *means = charge_dir->mkdir("means");
		TDirectory *sigmas = charge_dir->mkdir("sigmas");


	for(int hybrid=0; hybrid < hybrids; hybrid++){

		for(int stream=0; stream < 2; stream++){

			if(scan==3||scan==10){
				ht[Form("%ipointscan_R%iH%i_stream%i",scan, module,hybrid, stream)] = new TGraph();
        ht[Form("%ipointscan_R%iH%i_stream%i",scan, module,hybrid, stream)]->SetNameTitle(Form("%ipointscan_R%iH%i_stream%i_allchips",scan, module,hybrid, stream),Form("%ipointscan_R%iH%i_stream%i_allchips",scan, module,hybrid, stream));

				//tgraphs filled and written later
				for(int chip=1; chip<chips; chip++){
					ht[Form("%ipointscan_R%iH%i_stream%i_chip%i", scan, module,hybrid, stream, chip)] = new TGraph();
          ht[Form("%ipointscan_R%iH%i_stream%i_chip%i",scan, module, hybrid,stream, chip)]->SetNameTitle(Form("%ipointscan_R%iH%i_stream%i_chip%i",scan, module, hybrid, stream, chip),Form("%ipointscan_R%iH%i_stream%i_chip%i",scan, module,hybrid, stream, chip));
                        					//tgraphs filled and written later
				}
			}

			//fitted mean
        		h[Form("%i_h_mean_R%iH%i_stream%i",charge,module, hybrid, stream)] = (TH1F*)(data_file->Get(Form("h_mean%i;%i", stream, hybrid+1)));
       	 		h[Form("%i_h_mean_R%iH%i_stream%i",charge,module, hybrid, stream)]->SetNameTitle(Form("%i_h_mean_R%iH%i_stream%i",charge,module, hybrid, stream),Form("%i_h_mean_R%iH%i_stream%i",charge,module,hybrid, stream));
        		h[Form("%i_h_mean_R%iH%i_stream%i",charge,module, hybrid, stream)]->GetXaxis()->SetTitle("Channel");
        		h[Form("%i_h_mean_R%iH%i_stream%i",charge,module, hybrid, stream)]->GetYaxis()->SetTitleOffset(1);
        		h[Form("%i_h_mean_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetYaxis()->SetTitle("Mean (Vt50) from S Curve fit");
						h[Form("%i_h_mean_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetYaxis()->SetRangeUser(0,60);

        		charge_dir->cd();
						means->cd();
						h[Form("%i_h_mean_R%iH%i_stream%i",charge,module, hybrid, stream)]->Write();

			h[Form("%i_hist_mean_R%iH%i_stream%i",charge,module,hybrid, stream)]= new TH1F(Form("%i_hist_mean_R%iH%i_stream%i",charge,module, hybrid, stream),Form("%i_hist_mean_R%iH%i_stream%i",charge,module,hybrid, stream), 80, 0, 100);
			h[Form("%i_hist_mean_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetXaxis()->SetTitle("Mean Vt50 from S curve fit");
			h[Form("%i_hist_mean_R%iH%i_stream%i",charge,module, hybrid, stream)]->GetYaxis()->SetTitleOffset(1);
			h[Form("%i_hist_mean_R%iH%i_stream%i",charge,module, hybrid, stream)]->GetYaxis()->SetTitle("Occurences");


			///fitted mean: actual hists
			for(int chip = 1; chip<chips; chip++){
		//		printf("checking the made chip hists for module %i stream %i chip %i\n", 1-module, stream, chip);
				h[Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,module, hybrid, stream,chip)]= new TH1F(Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip),Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip), 140, 0, 100);
			//	h[Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,1-module, stream,chip)]->SetNameTitle(Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,1-module, stream,chip),Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,1-module, stream,chip));
				h[Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]->GetXaxis()->SetTitle("Mean Vt50 from S curve fit");
				h[Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]->GetYaxis()->SetTitleOffset(1);
				h[Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]->GetYaxis()->SetTitle("Occurences");

			/*	fits[Form("%i_hist_fit_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream, chip)] = new TF1(Form("%i_hist_fit_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream, chip),"TMath::Gaus",0,100);
				//fits[Form("%i_hist_fit_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream, chip)]->SetParameters(3000, 7, 8);
				if(verbose){h[Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream, chip)]->Fit(fits[Form("%i_hist_fit_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream, chip)], "R");}
				else if(!verbose){h[Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream, chip)]->Fit(fits[Form("%i_hist_fit_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream, chip)], "RQ");}
				fits[Form("%i_hist_fit_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream, chip)]->Draw();
*/
			}

			//fitted sigma
	        	h[Form("%i_h_sigma_R%iH%i_stream%i",charge, module, hybrid, stream)] = (TH1F*)(data_file->Get(Form("h_sigma%i;%i", stream, hybrid+1)));
	        	h[Form("%i_h_sigma_R%iH%i_stream%i",charge, module, hybrid, stream)]->SetNameTitle(Form("%i_h_sigma_R%iH%i_stream%i",charge,module,hybrid ,stream),Form("%i_h_sigma_R%iH%i_stream%i",charge,module,hybrid, stream));
						h[Form("%i_h_sigma_R%iH%i_stream%i",charge, module, hybrid, stream)]->GetXaxis()->SetTitle("Channel");
	        	h[Form("%i_h_sigma_R%iH%i_stream%i",charge, module, hybrid, stream)]->GetYaxis()->SetTitleOffset(1);
	        	h[Form("%i_h_sigma_R%iH%i_stream%i",charge, module, hybrid, stream)]->GetYaxis()->SetTitle("Sigma (output noise) from S Curve fit");
						h[Form("%i_h_sigma_R%iH%i_stream%i",charge, module, hybrid, stream)]->GetYaxis()->SetRangeUser(0,6);

	        	charge_dir->cd();
						sigmas->cd();
						h[Form("%i_h_sigma_R%iH%i_stream%i",charge, module, hybrid, stream)]->Write();


			h[Form("%i_hist_sigma_R%iH%i_stream%i",charge,module,hybrid, stream)]= new TH1F(Form("%i_hist_sigma_R%iH%i_stream%i",charge,module,hybrid, stream),Form("%i_hist_sigma_R%iH%i_stream%i",charge,module,hybrid, stream), 150, 0, 20);
			h[Form("%i_hist_sigma_R%iH%i_stream%i",charge,module,hybrid, stream)]->SetNameTitle(Form("%i_hist_sigma_R%iH%i_stream%i",charge,module,hybrid, stream),Form("%i_hist_sigma_R%iH%i_stream%i",charge,module,hybrid, stream));
			h[Form("%i_hist_sigma_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetXaxis()->SetTitle("Mean Vt50 from S curve fit");
			h[Form("%i_hist_sigma_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetYaxis()->SetTitleOffset(1);
			h[Form("%i_hist_sigma_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetYaxis()->SetTitle("Occurences");



			///fitted sigma: actual hists
			for(int chip = 1; chip<chips; chip++){
				h[Form("%i_hist_sigma_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]= new TH1F(Form("%i_hist_sigma_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip),Form("%i_hist_sigma_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip), 100, 0, 20);
		//		printf("chip loop: %i ", chip);
				h[Form("%i_hist_sigma_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]->SetNameTitle(Form("%i_hist_sigma_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip),Form("%i_hist_sigma_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip));
				h[Form("%i_hist_sigma_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]->GetXaxis()->SetTitle("Noise from S curve fit");
				h[Form("%i_hist_sigma_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]->GetYaxis()->SetTitleOffset(1);
				h[Form("%i_hist_sigma_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]->GetYaxis()->SetTitle("Occurences");

				//filling the actual hists with sigma and mean values
				for(int channel = limits[chip].at(0); channel<limits[chip].at(1);channel++){
					//printf("channel loop: %i", channel);
					float value_sigma = h[Form("%i_h_sigma_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetBinContent(channel);
					float value = h[Form("%i_h_mean_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetBinContent(channel);
					h[Form("%i_hist_sigma_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]->Fill(value_sigma);
					h[Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]->Fill(value);
					h[Form("%i_hist_mean_R%iH%i_stream%i",charge,module,hybrid, stream)]->Fill(value);
					h[Form("%i_hist_sigma_R%iH%i_stream%i",charge,module, hybrid,stream)]->Fill(value_sigma);
					//write histos
				}
				charge_dir->cd();
				sigmas->cd();
				//printf("here 10\n");
				h[Form("%i_hist_sigma_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]->Write();
			//	printf("here 11\n");
				charge_dir->cd();
				means->cd();

				h[Form("%i_hist_mean_R%iH%i_stream%i_chip%i",charge,module,hybrid, stream,chip)]->Write();
				charge_dir->cd();
			}



			means->cd();
		//	printf("here writing mean hist for charge %i module %i stream %i\n", charge, 1-module, stream);
			h[Form("%i_hist_mean_R%iH%i_stream%i",charge,module,hybrid, stream)]->Write();
			fout->cd();
			sigmas->cd();
		//	printf("here writing sigma hist for charge %i module %i stream %i\n", charge, 1-module, stream);
			h[Form("%i_hist_sigma_R%iH%i_stream%i",charge,module,hybrid, stream)]->Write();
	        	//scan histo
	        	h2[Form("%i_h_scan_R%iH%i_stream%i",charge,module,hybrid, stream)] = (TH2F*)(data_file->Get(Form("h_scan%i;%i", stream, hybrid+1)));
	       	 	h2[Form("%i_h_scan_R%iH%i_stream%i",charge,module,hybrid, stream)]->SetNameTitle(Form("%i_h_scan_R%iH%i_stream%i",charge,module,hybrid, stream),Form("%i_h_scan_R%iH%i_stream%i",charge,module,hybrid, stream));
	        	h2[Form("%i_h_scan_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetXaxis()->SetTitle("Channel");
	        	h2[Form("%i_h_scan_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetYaxis()->SetTitle("Threshold DAC Counts");
	        	h2[Form("%i_h_scan_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetYaxis()->SetTitleOffset(1);
	        	h2[Form("%i_h_scan_R%iH%i_stream%i",charge,module,hybrid, stream)]->GetZaxis()->SetTitle("Hits");
	        	charge_dir->cd();
						h2[Form("%i_h_scan_R%iH%i_stream%i",charge,module,hybrid, stream)]->Write();

			//y projection of scan histo
	        	hd[Form("%i_h_scan_yproj_R%iH%i_stream%i",charge,module,hybrid, stream)]=h2[Form("%i_h_scan_R%iH%i_stream%i",charge,module, hybrid,stream)]->ProjectionY();
	        	hd[Form("%i_h_scan_yproj_R%iH%i_stream%i",charge,module,hybrid, stream)]->SetNameTitle(Form("%i_h_scan_yproj_R%iH%i_stream%i",charge,module,hybrid, stream),Form("%i_h_scan_yproj_R%iH%i_stream%i",charge,module,hybrid, stream));
						hd[Form("%i_h_scan_yproj_R%iH%i_stream%i",charge,module,hybrid,stream)]->GetXaxis()->SetTitle("Threshold DAC Counts");
	        	hd[Form("%i_h_scan_yproj_R%iH%i_stream%i",charge,module,hybrid,stream)]->GetXaxis()->SetTitleOffset(1);
	        	hd[Form("%i_h_scan_yproj_R%iH%i_stream%i",charge,module,hybrid,stream)]->GetYaxis()->SetTitleOffset(1);
	        	hd[Form("%i_h_scan_yproj_R%iH%i_stream%i",charge,module,hybrid,stream)]->GetYaxis()->SetTitle("Hits");

			//fitting the s curve
			fits[Form("%i_h_scan_fit_yproj_R%iH%i_stream%i",charge,module,hybrid, stream)] = new TF1(Form("%i_h_scan_fit_yproj_R%iH%i_stream%i",charge,module,hybrid, stream),"([0]/2)*(1+TMath::Erf(([1]-x)/[2]))",0,250);
			fits[Form("%i_h_scan_fit_yproj_R%iH%i_stream%i",charge,module,hybrid, stream)]->SetParameters(3000, 7, 8);
			if(verbose){hd[Form("%i_h_scan_yproj_R%iH%i_stream%i",charge,module,hybrid, stream)]->Fit(fits[Form("%i_h_scan_fit_yproj_R%iH%i_stream%i",charge,module,hybrid, stream)], "R");}
			else if(!verbose){hd[Form("%i_h_scan_yproj_R%iH%i_stream%i",charge,module,hybrid, stream)]->Fit(fits[Form("%i_h_scan_fit_yproj_R%iH%i_stream%i",charge,module,hybrid, stream)], "RQ");}
			fits[Form("%i_h_scan_fit_yproj_R%iH%i_stream%i",charge,module,hybrid, stream)]->Draw();
			//fits[Form("%i_h_scan_fit_yproj_R%iH%i_stream%i",charge,1-module, stream)]->Write();
			//charge_dir->cd();
			hd[Form("%i_h_scan_yproj_R%iH%i_stream%i",charge,module,hybrid, stream)]->Write();


			}   //stream
		}  //hybrid

}  //file
return 1;
}  //histthresh::make

void HistThresh::Scan(int n, int module, int hybrids, int chips){
if(n==3){charges= {105, 210, 315};}
        else if(n==10){charges= {1025, 2050, 3075, 4100, 5125, 6150, 7200, 8300, 9400, 9600};}


//////////////////////////////////////
htfit[Form("%ipointscan_fit_data_gains", scan)] = new TGraph();
htfit[Form("%ipointscan_fit_data_gains", scan)]->SetNameTitle(Form("%ipointscan_fit_data_gains",scan),Form("%ipointscan_fit_data_gains",scan));

htfit[Form("%ipointscan_fit_data_intercepts", scan)] = new TGraph();
htfit[Form("%ipointscan_fit_data_intercepts", scan)]->SetNameTitle(Form("%ipointscan_fit_data_intercepts",scan),Form("%ipointscan_fit_data_intercepts",scan));
/*
htfit[Form("%ipointscan_fit_data_gains_allchips", scan)] = new TGraph();
htfit[Form("%ipointscan_fit_data_gains_allchips", scan)]->SetNameTitle(Form("%ipointscan_fit_data_gains_allchips",scan),Form("%ipointscan_fit_data_gains_allchips",scan));

htfit[Form("%ipointscan_fit_data_intercepts_allchips", scan)] = new TGraph();
htfit[Form("%ipointscan_fit_data_intercepts_allchips", scan)]->SetNameTitle(Form("%ipointscan_fit_data_intercepts_allchips",scan),Form("%ipointscan_fit_data_intercepts_allchips",scan));
*/
///////////////////////////////////////////
//int module=2;
	for(int hybrid=0; hybrid < hybrids; hybrid++){
		for(int stream=0; stream < 2; stream++){
			for(int file =0; file<n; file++){
				int charge = charges.at(file);
//cout<<"3ptgain"<<endl;
				h[Form("%i_hist_mean_R%iH%i_stream%i", charge, module,hybrid, stream)]=new TH1F(Form("%i_hist_mean_R%iH%i_stream%i", charge, module,hybrid, stream), Form("%i_hist_mean_R%iH%i_stream%i", charge, module,hybrid, stream), 1000, 0, 200);
				h[Form("%i_hist_mean_R%iH%i_stream%i", charge, module, hybrid, stream)]->GetXaxis()->SetTitle("Mean Vt50 from S curve fit");
				h[Form("%i_hist_mean_R%iH%i_stream%i", charge, module, hybrid, stream)]->GetYaxis()->SetTitle("Occurences");
				counter[Form("%i%i%i", file,stream,hybrid )]=0;
				for(int chip =1; chip<chips;chip++){
					counter[Form("%i%i%i%i", file,stream, hybrid, chip )]=0;
					hextra[Form("file%ihyb%istream%ichip%i", file, hybrid, stream, chip)]=(TH1F*) h[Form("%i_h_mean_R%iH%i_stream%i",charge,module,hybrid, stream)]->Clone();
					hextra[Form("file%ihyb%istream%ichip%i", file, hybrid, stream, chip)]->SetNameTitle(Form("file%ihyb%istream%ichip%i", file, hybrid, stream, chip),Form("file%ihyb%istream%ichip%i", file, hybrid, stream, chip));
					hextra[Form("file%ihyb%istream%ichip%i",file, hybrid, stream, chip)]->GetXaxis()->SetRangeUser(limits[chip].at(0), limits[chip].at(1));
					for(int channel = limits[chip].at(0); channel<limits[chip].at(1); channel++){
						int k = hextra[Form("file%ihyb%istream%ichip%i", file, hybrid, stream, chip)]->GetXaxis()->FindBin(channel);
						counter[Form("%i%i%i",file, stream, hybrid)]=counter[Form("%i%i%i",file, stream, hybrid)]+hextra[Form("file%ihyb%istream%ichip%i", file, hybrid, stream, chip)]->GetBinContent(k);
						counter[Form("%i%i%i%i",file, stream, hybrid, chip)]=counter[Form("%i%i%i%i",file, stream,hybrid, chip)]+hextra[Form("file%ihyb%istream%ichip%i", file, hybrid, stream, chip)]->GetBinContent(k);
//						printf("doing the thing file %i stream %i module %i chip%i bin (%i) content for channel %i: %i, total %i charge %f\n" , file, stream, module, chip,k,channel, hextra[Form("file%imod%istream%ichip%i", file, module, stream    , chip)]->GetBinContent(k), counter[Form("%i%i%i",file, stream, module)], charge);
					}
				}
			fout->cd();
			}
		}
	}
	TDirectory *treptgain = fout->mkdir("treptgain");
	treptgain->cd();
       	if(n==10){charge_vals= vector<float>({0.25, 0.5, 0.75, 1, 1.25, 1.5, 2, 3, 4, 6});}
	else if(n==3){charge_vals= vector<float>({0.5,  1, 1.5});}
			for(int hybrid=0; hybrid<hybrids; hybrid++){
    		for(int stream=0; stream<2; stream++){
				for(int file =0; file<n; file++){
					//cout<<"3ptgain"<<endl;
//					printf("making point for histo %ipointscan_R%iH%i_stream%i: counter %i, x: %f, y: %f (note: thing: %i  divided by 256: %f)\n",n, module, stream,file,charges.at(file),counter[Form("%i%i%i", file,stream, module)]/256, counter[Form("%i%i%i", file,stream,     module)], counter[Form("%i%i%i", file,stream,     module)]/256);
					ht[Form("%ipointscan_R%iH%i_stream%i", n, module, hybrid, stream)]->SetPoint(file,charge_vals.at(file),counter[Form("%i%i%i", file,stream, hybrid)]/128.0);

					fits[Form("linear_fit_%i%i%i", n, stream,hybrid)]=new TF1(Form("%i%i%i", n, stream, hybrid), "[0]+[1]*x",0, 10);
					ht[Form("%ipointscan_R%iH%i_stream%i", n, module, hybrid, stream)]->Fit(fits[Form("linear_fit_%i%i%i", n, stream,hybrid)], "Q");
					fits[Form("linear_fit_%i%i%i", n, stream, hybrid)]->Draw();
					//ht[Form("%ipointscan_R%iH%i_stream%i", n, module, hybrid, stream)]->Write();
		//			ht[Form("%ipointscan_R%iH%i_stream%i_chip%i", n, module, stream, chip)]->Fit("pol1");

					float intercept = fits[Form("linear_fit_%i%i%i", n, stream, hybrid)]->GetParameter(0);
					float gain = fits[Form("linear_fit_%i%i%i", n, stream, hybrid)]->GetParameter(1);
	/*				htfit[Form("%ipointscan_fit_data_gains_allchips", n)]->SetPoint(hybrid*2+stream+1,hybrid*2+stream,gain);
					htfit[Form("%ipointscan_fit_data_intercepts_allchips", n)]->SetPoint(hybrid*2+stream+1,hybrid*2+stream,intercept);
					htfit[Form("%ipointscan_fit_data_gains_allchips", scan)]->GetXaxis()->SetTitle("Hybrid x 2 + Stream");
					htfit[Form("%ipointscan_fit_data_gains_allchips", scan)]->GetYaxis()->SetTitle("Gain [DAC Counts/fC]");
					htfit[Form("%ipointscan_fit_data_gains_allchips", scan)]->SetMarkerStyle(20);
					htfit[Form("%ipointscan_fit_data_intercepts_allchips", scan)]->GetXaxis()->SetTitle("Hybrid x 2 + Stream");
					htfit[Form("%ipointscan_fit_data_intercepts_allchips", scan)]->GetYaxis()->SetTitle("Intercept [DAC Counts]");
					htfit[Form("%ipointscan_fit_data_intercepts_allchips", scan)]->SetMarkerStyle(20);
*/
					for(int chip=1; chip<chips; chip++){
						ht[Form("%ipointscan_R%iH%i_stream%i_chip%i", n, module, hybrid,stream, chip)]->SetPoint(file,charge_vals.at(file),counter[Form("%i%i%i%i", file,stream, hybrid, chip)]/128.0);
					//	printf("Adding point to %ipointscan_R%iH%i_stream%i_chip%i------- number %i for charge %f value %f without div: %i\n", n, module, stream, chip, file, charge_vals.at(file) , counter[Form("%i%i%i%i", file,stream, module, chip)]/256, counter[Form("%i%i%i%i", file,stream, module, chip)] );
//						fits[Form("%i%i%i%i", n, stream, module, chip)]=new TH1(Form("%i%i%i%i", n, stream, module, chip), "pol1");
					}
			}

			ht[Form("%ipointscan_R%iH%i_stream%i",scan, module, hybrid,stream)]->GetXaxis()->SetTitle("Charge [fC]");
			ht[Form("%ipointscan_R%iH%i_stream%i", scan,module,hybrid, stream)]->GetYaxis()->SetTitle("Mean Vt50 [DAC Counts]");
			ht[Form("%ipointscan_R%iH%i_stream%i", scan,module,hybrid, stream)]->SetMarkerStyle(20);
			ht[Form("%ipointscan_R%iH%i_stream%i", n,module,hybrid, stream)]->Write();
			for(int chip = 1; chip<chips;  chip++){
				//cout<<"3ptgain"<<endl;
				//ht[Form("%ipointscan_R%iH%i_stream%i_chip%i", n, module, stream, chip)]->SetPoint(file,charge_vals.at(file),counter[Form("%i%i%i%i", file,stream, module, chip)]/256);

				fits[Form("linear_fit_%i%i%i%i", n, stream,hybrid, chip)]=new TF1(Form("%i%i%i%i", n, stream, hybrid, chip), "[0]+[1]*x",0, 10);
				ht[Form("%ipointscan_R%iH%i_stream%i_chip%i", n, module,hybrid, stream, chip)]->Fit(fits[Form("linear_fit_%i%i%i%i", n, stream, hybrid, chip)], "Q");
				fits[Form("linear_fit_%i%i%i%i", n, stream, hybrid, chip)]->Draw();
		//		ht[Form("%ipointscan_R%iH%i_stream%i_chip%i", n, module, stream, chip)]->Fit("pol1");
				float intercept = fits[Form("linear_fit_%i%i%i%i", n, stream, hybrid, chip)]->GetParameter(0);
				float gain = fits[Form("linear_fit_%i%i%i%i", n, stream, hybrid, chip)]->GetParameter(1);
				htfit[Form("%ipointscan_fit_data_gains", n)]->SetPoint(hybrid*12+stream*6+chip-1, hybrid*12+stream*6+chip,gain);
				htfit[Form("%ipointscan_fit_data_intercepts", n)]->SetPoint(hybrid*12+stream*6+chip-1, hybrid*12+stream*6+chip,intercept);
				htfit[Form("%ipointscan_fit_data_gains", scan)]->GetXaxis()->SetTitle("Hybrid (x2) + Stream (x4) + Chip (x8/9)");
				htfit[Form("%ipointscan_fit_data_gains", scan)]->GetYaxis()->SetTitle("Gain - Slope [DAC Counts/fC]");
				htfit[Form("%ipointscan_fit_data_gains", scan)]->GetYaxis()->SetRangeUser(0,40);
				htfit[Form("%ipointscan_fit_data_gains", scan)]->SetMarkerStyle(20);
				htfit[Form("%ipointscan_fit_data_intercepts", scan)]->GetXaxis()->SetTitle("Hybrid (x2) + Stream (x4) + Chip (x8/9)");
				htfit[Form("%ipointscan_fit_data_intercepts", scan)]->GetYaxis()->SetTitle("Offset - Intercept [DAC Counts]");
				htfit[Form("%ipointscan_fit_data_intercepts", scan)]->GetYaxis()->SetRangeUser(0,20);
				htfit[Form("%ipointscan_fit_data_intercepts", scan)]->SetMarkerStyle(20);
				ht[Form("%ipointscan_R%iH%i_stream%i_chip%i", scan, module, hybrid,stream, chip)]->GetXaxis()->SetTitle("Charge [fC]");
				ht[Form("%ipointscan_R%iH%i_stream%i_chip%i", scan, module, hybrid,stream, chip)]->GetYaxis()->SetTitle("Mean Vt50 [DAC Counts]");
				ht[Form("%ipointscan_R%iH%i_stream%i_chip%i", scan, module, hybrid,stream, chip)]->SetMarkerStyle(20);
				ht[Form("%ipointscan_R%iH%i_stream%i_chip%i", n, module, hybrid, stream, chip)]->Write();
				//ht[Form("%ipointscan_fit_data_gains", n)]->Write();
				//ht[Form("%ipointscan_fit_data_intercepts", n)]->Write();
			}
			//fout->cd();

		}
	}

}  //histThreah::Scan


void HistThresh::GetSCurve(int module, int hybrid, int stream, int channel, int chip){
	if(channel > 0){  //only a specific channel should be displayed
		printf("Displaying S Curve for module R%iH%i, stream %i, channel %i \n",module,hybrid, stream, channel);
        	h2[Form("h_scan_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]= (TH2F*) h2[Form("h_scan_R%iH%i_stream%i",module,hybrid, stream)]->Clone();

		//making it aesthetically clear
		h2[Form("h_scan_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->SetNameTitle(Form("h_scan_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel),Form("h_scan_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel));
        	h2[Form("h_scan_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->GetXaxis()->SetTitle("Channel");
        	h2[Form("h_scan_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->GetYaxis()->SetTitle("Threshold DAC Counts");
        	h2[Form("h_scan_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->GetYaxis()->SetTitleOffset(1);
        	h2[Form("h_scan_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->GetZaxis()->SetTitle("Hits");

		h2[Form("h_scan_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->GetXaxis()->SetRangeUser(channel, channel +1);
        	hd[Form("h_scan_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]=(TH1D*) h2[Form("h_scan_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->ProjectionY();

		//making it aesthetically clear
		hd[Form("h_scan_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->SetNameTitle(Form("h_scan_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel),Form("h_scan_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel));
        	hd[Form("h_scan_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->GetXaxis()->SetTitle("Threshold DAC Counts");
        	hd[Form("h_scan_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->GetXaxis()->SetTitleOffset(1);
        	hd[Form("h_scan_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->GetYaxis()->SetTitleOffset(1);
        	hd[Form("h_scan_yproj_R%iH%i_stream%i_channel%i",module, hybrid,stream, channel)]->GetYaxis()->SetTitle("Hits");

		//fitting
		fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)] = new TF1(Form("h_scan_fit_yproj_R%iH%i_stream%i_channel%i",module, hybrid,stream, channel),"([0]/2)*(1+TMath::Erf(([1]-x)/[2]))",0,250);
		fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->SetParameters(30, 7, 8);
		printf("\n----------Fitting h_scan_yproj_R%iH%i_stream%i_channel%i \n",module,hybrid, stream, channel);
		if(verbose){hd[Form("h_scan_yproj_R%iH%i_stream%i_channel%i",module, hybrid,stream, channel)]->Fit(fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_channel%i",module, hybrid,stream, channel)], "R");}
		else if(!verbose){hd[Form("h_scan_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->Fit(fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)], "RQ");}
		fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->Draw();
		hd[Form("h_scan_yproj_R%iH%i_stream%i_channel%i",module,hybrid, stream, channel)]->Write();




	}
	else if(chip > 0){ //a whole chip should be displayed
		printf("Displaying S Curve for module R%iH%i, stream %i, chip %i \n", module,hybrid, stream, chip);
        	h2[Form("h_scan_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]= (TH2F*) h2[Form("h_scan_R%iH%i_stream%i",module,hybrid, stream)]->Clone();

		//making it aesthetically clear
		h2[Form("h_scan_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->SetNameTitle(Form("h_scan_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip),Form("h_scan_R%iH%i_stream%i_chip%i",module, hybrid,stream, chip));

		h2[Form("h_scan_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->GetXaxis()->SetTitle("Channel");
        	h2[Form("h_scan_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->GetYaxis()->SetTitle("Threshold DAC Counts");
        	h2[Form("h_scan_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->GetYaxis()->SetTitleOffset(1);
        	h2[Form("h_scan_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->GetZaxis()->SetTitle("Hits");

		h2[Form("h_scan_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->GetXaxis()->SetRangeUser(limits[chip].at(0), limits[chip].at(1));
        	hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]=(TH1D*) h2[Form("h_scan_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->ProjectionY();

		//making it aesthetically clear
		hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->SetNameTitle(Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip),Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip));
		hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module, hybrid,stream, chip)]->GetXaxis()->SetTitle("Threshold DAC Counts");
        	hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->GetXaxis()->SetTitleOffset(1);
        	hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->GetYaxis()->SetTitleOffset(1);
        	hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->GetYaxis()->SetTitle("Hits");


		//fitting the s curve
		fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)] = new TF1(Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip),"([0]/2)*(1+TMath::Erf(([1]-x)/[2]))",0,250);
		fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module, hybrid,stream, chip)]->SetParameters(800, 7, 8);
		printf("\n----------Fitting h_scan_yproj_R%iH%i_stream%i_chip%i \n",module,hybrid, stream, chip);
		if(verbose){hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->Fit(fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)], "R");}
		else if(!verbose){hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->Fit(fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)], "RQ");}
		fits[Form("h_scan_fit_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->Draw();
		hd[Form("h_scan_yproj_R%iH%i_stream%i_chip%i",module,hybrid, stream, chip)]->Write();




	}
}

void HistThresh::writeHist(){
	for ( auto itt:htfit ){
                //std::cout<<"tgraPHS COMING IN!!!!! "<< itt.first << std::endl;
                itt.second->Write();
        }

	//for ( auto it:h ){
        //        std::cout<<"h: " << it.first << std::endl;
        //        it.second->Write();
        //}
  /*      for ( auto it2:h2 ){
               std::cout << "h2: " << it2.first << std::endl;
                it2.second->Write();
        }
        for ( auto itd:hd ){
                std::cout << "hd: "<< itd.first << std::endl;
                itd.second->Write();
        }*/
///////////////
/*TDirectory *treptgain = fout->mkdir("treptgain");

treptgain->cd();
for ( auto it:ht ){
			//        std::cout<<"hextra crap: " << itextra.first << std::endl;
							it.second->Write();
			}*/

	TDirectory *extra = fout->mkdir("extra");
	extra->cd();
	for ( auto itextra:hextra ){
        //        std::cout<<"hextra crap: " << itextra.first << std::endl;
                itextra.second->Write();
        }
	fout->cd();

	fout->Close();
}

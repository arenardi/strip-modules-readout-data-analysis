//HistStrobeComp.cpp
#include "HistStrobeComp.h"
#include <cmath>
#include <TMath.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TColor.h>
#include <TDirectory.h>
using namespace std;

//TDirectory *cdtof = top->mkdir("tof");
//cdtof->cd()
namespace {
	template <typename T>
	std::ostream& operator<<(std::ostream& os, const vector<T>& v) {
		os << "[";
		if (v.size() > 0)
			os << v.at(0);
		for (auto itr = v.begin() + 1; itr != v.end(); ++itr) {
			os << ", " << *itr;
		}
		os << "]";
	}

	void printParams(TF1* f) {
		std::vector<Double_t> params(f->GetNpar() );
		f->GetParameters(params.data() );
		std::cout << params << std::endl;
	}
}


int HistStrobeComp::make(vector<TFile*> input, int modules, int hybrids,  bool v){
	vector<TFile*> data_files = input;
	data_files_size = data_files.size();
	modules = modules;
	verbose = v;
	for(int file=0; file<data_files.size();file++){
		TFile *data_file = data_files.at(file);
		ht[Form("optimal_values_file%i", file)]=(TGraph*)data_file->Get("h_optimal_values");

			for(int module=0; module<modules; module++){
				for(int hybrid=0; hybrid<hybrids; hybrid++){
					for(int stream=0; stream<2; stream++){
					data_file->cd();
					data_file->cd("means/");
					//cout << ">>>>means current directory: ";
					//gDirectory->pwd()
					h[Form("file%i_hist_mean_R%iH%i_stream%i_allchips", file+1, module, hybrid, stream)]=(TH1F*)gDirectory->Get(Form("hist_mean_R%iH%i_stream%i", module, hybrid, stream));
					h[Form("file%i_hist_mean_R%iH%i_stream%i_allchips", file+1, module, hybrid, stream)]->SetNameTitle(Form("file%i_hist_mean_R%iH%i_stream%i_allchips", file+1, module, hybrid, stream), Form("file%i_hist_mean_R%iH%i_stream%i_allchips", file+1, module, hybrid, stream));
						for(int chip=1;chip< hybrid+9;chip++){
							h[Form("file%i_hist_mean_R%iH%i_stream%i_chip%i", file+1,  module, hybrid, stream, chip)]=(TH1F*)gDirectory->Get(Form("hist_mean_R%iH%i_stream%i_chip%i",  module, hybrid, stream, chip));
							h[Form("file%i_hist_mean_R%iH%i_stream%i_chip%i", file+1,  module, hybrid, stream, chip)]->SetNameTitle(Form("file%i_hist_mean_R%iH%i_stream%i_chip%i", file+1,  module, hybrid, stream, chip), Form("file%i_hist_mean_R%iH%i_stream%i_chip%i", file+1, module, hybrid, stream, chip));

						}


						data_file->cd();
						data_file->cd("sigmas/");
						//cout << ">>>> sigmas current directory: ";
						//gDirectory->pwd();
						hsig[Form("file%i_hist_sigma_R%iH%i_stream%i_allchips", file+1, module, hybrid, stream)]=(TH1F*)gDirectory->Get(Form("hist_sigma_R%iH%i_stream%i",  module, hybrid, stream));
						hsig[Form("file%i_hist_sigma_R%iH%i_stream%i_allchips", file+1, module, hybrid, stream)]->SetNameTitle(Form("file%i_hist_sigma_R%iH%i_stream%i_allchips", file+1,  module, hybrid, stream), Form("file%i_hist_sigma_R%iH%i_stream%i_allchips", file+1,  module, hybrid, stream));

						for(int chip=1;chip< hybrid+9;chip++){
							hsig[Form("file%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1, module, hybrid, stream, chip)]=(TH1F*)gDirectory->Get(Form("hist_sigma_R%iH%i_stream%i_chip%i",  module, hybrid, stream, chip));
							hsig[Form("file%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1, module, hybrid, stream, chip)]->SetNameTitle(Form("file%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1,  module, hybrid, stream, chip), Form("file%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1,  module, hybrid, stream, chip));
						}


						data_file->cd();
					 	data_file->cd("scan/");
					 	h2[Form("file%i_h_scan_yproj_R%iH%i_stream%i", file+1, module, hybrid, stream)]=(TH2F*)gDirectory->Get(Form("h_scan_yproj_R%iH%i_stream%i",  module, hybrid, stream));
				 		h2[Form("file%i_h_scan_yproj_R%iH%i_stream%i", file+1, module, hybrid, stream)]->SetNameTitle(Form("file%i_h_scan_yproj_R%iH%i_stream%i", file+1,  module, hybrid, stream), Form("file%i_h_scan_yproj_R%iH%i_stream%i", file+1,  module, hybrid, stream));

				 		for(int chip=1;chip< hybrid+9;chip++){
				 			hd[Form("file%i_h_scan_yproj_R%iH%i_stream%i_chip%i", file+1, module, hybrid, stream, chip)]=(TH1D*)gDirectory->Get(Form("h_scan_yproj_R%iH%i_stream%i_chip%i",  module, hybrid, stream, chip));
				 			hd[Form("file%i_h_scan_yproj_R%iH%i_stream%i_chip%i", file+1, module, hybrid, stream, chip)]->SetNameTitle(Form("file%i_h_scan_yproj_R%iH%i_stream%i_chip%i", file+1,  module, hybrid, stream, chip), Form("file%i_h_scan_yproj_R%iH%i_stream%i_chip%i", file+1,  module, hybrid, stream, chip));
				 		}


				}
			}}
	}
return 1;
}

void HistStrobeComp::Compare(vector<string> labels, vector<TFile*> data_files, int modules, int hybrids, int chip_to_show, bool allchips){

vector<int>  colors = vector<int>({kViolet+4, kSpring-5, kPink-3, kYellow-2, kBlue});

//MEANS-means////////////////////////compound pdfs////////////////////////////////////////////////////////
	printf("Running the compare function - means");
	canvas_map["strobe"]=new TCanvas("strobe", "strobe",1);
	canvas_map["strobe"]->Divide(4,2);
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
			for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
						canvas_map["strobe"]->cd(hybrid*2+stream+1);
						//canvas_map["means"]->cd(hybrid*2+stream+1+4*scan_val)->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ));
						legend_map[Form("means_R%iH%i_stream%i",module, hybrid, stream)] = new TLegend(0.3,0.8,0.9,0.9);
						legend_map[Form("means_R%iH%i_stream%i",module, hybrid, stream)]->SetHeader(Form("Means R%iH%i Stream %i", module, hybrid, stream ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]=(TH1F*)h[Form("file%i_hist_mean_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->Clone();
						hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->SetNameTitle(Form("means: R%iH%i Stream%i", module, hybrid, stream ),Form("means:  R%iH%i Stream%i", module, hybrid, stream ));
						hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->SetLineColor(colors.at(file));
						hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->SetStats(0);
						hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->GetXaxis()->SetRangeUser(30, 90);
						//hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->GetXaxis()->SetRangeUser(0, 150);
						hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->SetLineWidth(1);
						hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->Draw("SAME");
  		      				legend_map[Form("means_R%iH%i_stream%i",module, hybrid, stream)]->AddEntry(hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips", file+1,module, hybrid, stream)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("means_R%iH%i_stream%i",module, hybrid, stream)]->Draw();


		}
	}}



//MEANS-means//////////////////////individual canvases//////////////////////////////////////////////////
	printf("Running the compare function - individual means");
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
			for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
						canvas_map_in[Form("means_R%iH%i_stream%i",module, hybrid, stream)]=new TCanvas(Form("means_R%iH%i_stream%i", module, hybrid, stream),Form("means_R%iH%i_stream%i", module, hybrid, stream), 1);
						legend_map[Form("means_R%iH%i_stream%i",module, hybrid, stream)] = new TLegend(0.3,0.8,0.9,0.9);
						legend_map[Form("means_R%iH%i_stream%i",module, hybrid, stream)]->SetHeader(Form("Means R%iH%i Stream %i", module, hybrid, stream ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->Draw("SAME");
  		      				legend_map[Form("means_R%iH%i_stream%i",module, hybrid, stream)]->AddEntry(hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips", file+1, module, hybrid, stream)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("means_R%iH%i_stream%i",module, hybrid, stream)]->Draw();


		}
}	}

//SIGMAS////////////////////////compound pdfs////////////////////////////////////////////////////////
	printf("Running the compare function - SIGMAS");
	//canvas_map["sigmas"]=new TCanvas("sigmas", "sigmas",1);
	//canvas_map["sigmas"]->Divide(4);
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
						//canvas_map["sigmas"]->cd(hybrid*2+stream+1);
						canvas_map["strobe"]->cd(hybrid*2+stream+1+4);
						legend_map[Form("sigmas_R%iH%i_stream%i",module, hybrid, stream)] = new TLegend(0.3,0.8,0.9,0.9);
						legend_map[Form("sigmas_R%iH%i_stream%i",module, hybrid, stream)]->SetHeader(Form("Sigmas  R%iH%i Stream %i", module, hybrid, stream ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]=(TH1F*)hsig[Form("file%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->Clone();
						hextra[Form("clone_file%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->SetNameTitle(Form("SIGMAS: R%iH%i Stream%i", module, hybrid, stream ),Form("SIGMAS: R%iH%i Stream%i", module, hybrid, stream ));
						hextra[Form("clone_file%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->SetLineColor(colors.at(file));
						hextra[Form("clone_file%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->SetStats(0);
						hextra[Form("clone_file%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->SetLineWidth(1);
						hextra[Form("clone_file%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->Draw("SAME");
  		      				legend_map[Form("sigmas_R%iH%i_stream%i",module, hybrid, stream)]->AddEntry(hextra[Form("clone_file%i_hist_sigma_R%iH%i_stream%i_allchips", file+1, module, hybrid, stream)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("sigmas_R%iH%i_stream%i",module, hybrid, stream)]->Draw();


		}
	}}



//SIGMAS- SIGMAS//////////////////////individual canvases//////////////////////////////////////////////////
	printf("Running the compare function - individual SIGMAS");
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
			for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
						canvas_map_in[Form("sigmas_R%iH%i_stream%i",module, hybrid, stream)]=new TCanvas(Form("sigmas_R%iH%i_stream%i", module, hybrid, stream),Form("sigmas_R%iH%i_stream%i", module, hybrid, stream), 1);
						legend_map[Form("sigmas_R%iH%i_stream%i",module, hybrid, stream)] = new TLegend(0.3,0.8,0.9,0.9);
						legend_map[Form("sigmas_R%iH%i_stream%i",module, hybrid, stream)]->SetHeader(Form("Sigmas R%iH%i Stream %i", module, hybrid, stream ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_hist_sigma_R%iH%i_stream%i_allchips",file+1,module, hybrid, stream)]->Draw("SAME");
  		      				legend_map[Form("sigmas_R%iH%i_stream%i",module, hybrid, stream)]->AddEntry(hextra[Form("clone_file%i_hist_sigma_R%iH%i_stream%i_allchips", file+1, module, hybrid, stream)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("sigmas_R%iH%i_stream%i",module, hybrid, stream)]->Draw();


		}}
	}

//Strobe delay///////////////////
printf("Running the compare function - Y projection scan");
canvas_map["pscan"]=new TCanvas("pscan", "pscan",1);
canvas_map["pscan"]->Divide(4);
for(int module =0; module<modules; module++){
	for(int hybrid=0; hybrid<hybrids; hybrid++){
		for(int stream =0; stream<2; stream++){
		//printf("scan %i size of data files %i\n", scan, data_files_size);
					canvas_map["pscan"]->cd(hybrid*2+stream+1);
					//canvas_map["means"]->cd(hybrid*2+stream+1+4*scan_val)->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ));
					legend_map[Form("pscan_R%iH%i_stream%i",module, hybrid, stream)] = new TLegend(0.3,0.8,0.9,0.9);
					legend_map[Form("pscan_R%iH%i_stream%i",module, hybrid, stream)]->SetHeader(Form("PScans R%iH%i Stream %i", module, hybrid, stream ),"C");


				for(int file =0; file< data_files_size; file++){

					hextra[Form("clone_file%i_h_scan_yproj_R%iH%i_stream%i",file+1, module, hybrid, stream)]=(TH1F*)h2[Form("file%i_h_scan_yproj_R%iH%i_stream%i",file+1, module, hybrid, stream)]->Clone();
					hextra[Form("clone_file%i_h_scan_yproj_R%iH%i_stream%i",file+1, module, hybrid, stream)]->SetNameTitle(Form("PScans R%iH%i Stream %i", module, hybrid, stream ),Form("PScans R%iH%i Stream %i", module, hybrid, stream ));
					hextra[Form("clone_file%i_h_scan_yproj_R%iH%i_stream%i",file+1, module, hybrid, stream)]->SetLineColor(colors.at(file));
					hextra[Form("clone_file%i_h_scan_yproj_R%iH%i_stream%i",file+1, module, hybrid, stream)]->SetStats(0);
					hextra[Form("clone_file%i_h_scan_yproj_R%iH%i_stream%i",file+1, module, hybrid, stream)]->GetXaxis()->SetRangeUser(0, 70);
					//hextra[Form("clone_file%i_hist_mean_R%iH%i_stream%i_allchips",file+1, module, hybrid, stream)]->GetXaxis()->SetRangeUser(0, 150);
					hextra[Form("clone_file%i_h_scan_yproj_R%iH%i_stream%i",file+1, module, hybrid, stream)]->SetLineWidth(1);
					hextra[Form("clone_file%i_h_scan_yproj_R%iH%i_stream%i",file+1, module, hybrid, stream)]->Draw("SAME");
									legend_map[Form("pscan_R%iH%i_stream%i",module, hybrid, stream)]->AddEntry(hextra[Form("clone_file%i_h_scan_yproj_R%iH%i_stream%i", file+1,module, hybrid, stream)],labels.at(file).c_str(),"l");
							}
				legend_map[Form("pscan_R%iH%i_stream%i",module, hybrid, stream)]->Draw();


	}
}}

//MEANS-means//////////////////////individual canvases//////////////////////////////////////////////////
printf("Running the compare function - individual pscan");
for(int module =0; module<modules; module++){
	for(int hybrid=0; hybrid<hybrids; hybrid++){
		for(int stream =0; stream<2; stream++){
		//printf("scan %i size of data files %i\n", scan, data_files_size);
					canvas_map_in[Form("PScans R%iH%i Stream %i",module, hybrid, stream)]=new TCanvas(Form("PScans R%iH%i Stream %i", module, hybrid, stream),Form("PScans R%iH%i Stream %i", module, hybrid, stream), 1);
					legend_map[Form("PScans R%iH%i Stream %i",module, hybrid, stream)] = new TLegend(0.3,0.8,0.9,0.9);
					legend_map[Form("PScans R%iH%i Stream %i",module, hybrid, stream)]->SetHeader(Form("PScans R%iH%i Stream %i", module, hybrid, stream ),"C");

				for(int file =0; file< data_files_size; file++){
					hextra[Form("clone_file%i_h_scan_yproj_R%iH%i_stream%i",file+1, module, hybrid, stream)]->Draw("SAME");
									legend_map[Form("PScans R%iH%i Stream %i",module, hybrid, stream)]->AddEntry(hextra[Form("clone_file%i_h_scan_yproj_R%iH%i_stream%i", file+1, module, hybrid, stream)],labels.at(file).c_str(),"l");
							}
				legend_map[Form("PScans R%iH%i Stream %i",module, hybrid, stream)]->Draw();

}
	}
}



//Optimal Value ////////////////////////////////////////////////////////////////////////
	printf("Running the compare function - optimal value ");
	canvas_map_h["optimal_values"]=new TCanvas("optimal_values","optimal_values", 1);
	legend_map["optimal_values"] = new TLegend(0.5,0.7,0.9,0.9);
	legend_map["optimal_values"]->SetHeader(Form("Optimal values"),"C");
	for(int file =0; file< data_files_size; file++){
	hop[Form("optimal_values_file%i", file)]=new TH1F(Form("optimal_values_file%i", file),Form("optimal_values_file%i", file),50,30,60);
		Double_t *gain = ht[Form("optimal_values_file%i",file)]->GetY();
		for(int module =0; module<modules; module++){
			for(int hybrid=0; hybrid<hybrids; hybrid++){
				for(int stream =0; stream<2; stream++){

					for(int chip=1;chip<hybrid+9;chip++){
						float floaty_bo = gain[hybrid*18+stream*9+chip-1];
						hop[Form("optimal_values_file%i", file)]->Fill(floaty_bo);
	      				}
				}
			}}
		hop[Form("optimal_values_file%i", file)]->SetLineColor(colors.at(file));
		hop[Form("optimal_values_file%i", file)]->SetStats(0);
		hop[Form("optimal_values_file%i", file)]->SetLineWidth(3);
		hop[Form("optimal_values_file%i", file)]->SetNameTitle("optimal_values","optimal_values");
		hop[Form("optimal_values_file%i", file)]->GetXaxis()->SetTitle("Optimal Values of strobe delay");
		hop[Form("optimal_values_file%i", file)]->GetYaxis()->SetTitle("Occurences");
		hop[Form("optimal_values_file%i", file)]->Draw("SAME");
 		legend_map["optimal_values"]->AddEntry(hop[Form("optimal_values_file%i", file)],labels.at(file).c_str(),"l");

}
	legend_map["optimal_values"]->Draw();


if(chip_to_show>0){CompareChip(labels, data_files, modules, hybrids, chip_to_show);}
if(allchips){for(int chip = 1; chip< 10; chip++){CompareChip(labels, data_files, modules, hybrids, chip);}}

}



void HistStrobeComp::CompareChip(vector<string> labels, vector<TFile*> data_files, int modules, int hybrids, int chip_to_show){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////           CHIP REQUESTED         /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		printf("--------Individual chip PDFs requested------------\n");

vector<int>  colors = vector<int>({kViolet+4, kSpring-5, kPink-3, kYellow-2, kBlue});


//MEANS-VT50////////////////////////compound pdfs chip////////////////////////////////////////////////////////
	printf("Running the compare function - VT50 for chip %i", chip_to_show);
	canvas_map[Form("VT50_chip%i", chip_to_show)]=new TCanvas( Form("VT50_chip%i", chip_to_show) ,Form("VT50_chip%i", chip_to_show), 1);
	canvas_map[Form("VT50_chip%i", chip_to_show)]->Divide(4,scan);
		for(int module =0; module<modules; module++){
			for(int hybrid=0; hybrid<hybrids; hybrid++){

		if(hybrid==0 && chip_to_show==9){continue;}
		for(int stream =0; stream<2; stream++){
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
						canvas_map[Form("VT50_chip%i", chip_to_show)]->cd(hybrid*2+stream+1+4*scan_val);
						legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]=(TH1F*)h[Form("file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Clone();
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetNameTitle(Form("VT50: charge%1.1f R%iH%i Stream%i chip %i", charge_val, module, hybrid, stream, chip_to_show),Form("VT50: charge%1.1f  R%iH%i Stream%i chip%i", charge_val, module, hybrid, stream, chip_to_show ));
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineColor(colors.at(file));
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetStats(0);
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->GetYaxis()->SetRangeUser(0, 45);
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->GetXaxis()->SetRangeUser(0, 150);
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineWidth(1);
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      				legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();


			}
		}
	}}


//MEANS-VT50//////////////////////individual canvases chip//////////////////////////////////////////////////
	printf("Running the compare function - individual VT50 - for chip %i", chip_to_show);
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
			if(hybrid==0 && chip_to_show==9){continue;}
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
	//			printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map_in[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]=new TCanvas(Form("means_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show),Form("means_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show), 1);
						legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.5,0.7,0.9,0.9);
						legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      				legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_mean_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("means_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();


			}
		}
	}}

//SIGMAS-output noise////////////////////////compound pdfs chip////////////////////////////////////////////////////////
	printf("Running the compare function - OUTPUT NOISE for chip %i", chip_to_show);
	canvas_map[Form("output_noise_chip%i", chip_to_show)]=new TCanvas(Form("output_noise_chip%i", chip_to_show), Form("output_noise_chip%i", chip_to_show),1);
	canvas_map[Form("output_noise_chip%i", chip_to_show)]->Divide(4,scan);
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
		if(hybrid==0 && chip_to_show==9){continue;}
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
	//			printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map[Form("output_noise_chip%i", chip_to_show)]->cd(hybrid*2+stream+1+4*scan_val);
						//canvas_map["means"]->cd(hybrid*2+stream+1+4*scan_val)->SetHeader(Form("Means charge %1.1f  R%iH%i Stream %i", charge_val, module, hybrid, stream ));
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.3,0.7,0.9,0.9);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Sigmas charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]=(TH1F*)hsig[Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Clone();
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetNameTitle(Form("OUTPUT NOISE: charge%1.1f R%iH%i Stream%i Chip%i", charge_val, module, hybrid, stream, chip_to_show ),Form("OUTPUT NOISE: charge%1.1f  R%iH%i Stream%i Chip%i", charge_val, module, hybrid, stream, chip_to_show ));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineColor(colors.at(file));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetStats(0);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetYaxis()->SetRangeUser(0, 45);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetXaxis()->SetRangeUser(0, 150);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineWidth(1);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      				legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();

}
			}
		}
	}



//SIGMAS- OUTPUT NOISE//////////////////////individual canvases chip//////////////////////////////////////////////////
	printf("Running the compare function - individual OUTPUT NOISE");
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
		if(hybrid==0 && chip_to_show==9){continue;}
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
	//			printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map_in[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]=new TCanvas(Form("sigmas_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show),Form("sigmas_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show), 1);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.3,0.5,0.9,0.9);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Sigmas charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      				legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();


			}
		}
	}}

//SIGMAS- INPUT  noise////////////////////////compound pdfs chip////////////////////////////////////////////////////////
	printf("Running the compare function - INPUT NOISE");
	printf("1\n");
	canvas_map[Form("input_noise_chip%i", chip_to_show)]=new TCanvas(Form("input_noise_chip%i", chip_to_show),Form("input_noise_chip%i", chip_to_show),1);
	canvas_map[Form("input_noise_chip%i", chip_to_show)]->Divide(4,scan);
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
			if(hybrid==0 && chip_to_show==9){continue;}
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
				//printf("charge %i charge val %f\t", charge, charge_val);
						canvas_map[Form("input_noise_chip%i", chip_to_show)]->cd(hybrid*2+stream+1+4*scan_val);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.3,0.5,0.9,0.9);
						legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Sigmas charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show ),"C");
				//	printf("1: %i, 2: %i\n", data_files.size(), data_files_size);
					for(int file =0; file< data_files_size; file++){
						TFile *data_file = data_files.at(file);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]=(TH1F*)hsig[Form("file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Clone();
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetNameTitle(Form("INPUT NOISE: charge%1.1f R%iH%i Stream%i Chip%i", charge_val, module, hybrid, stream, chip_to_show ),Form("INPUT NOISE: charge%1.1f  R%iH%i Stream%i Chip%i", charge_val, module, hybrid, stream, chip_to_show ));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineColor(colors.at(file));
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetStats(0);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetYaxis()->SetRangeUser(0, 45);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_allchips",file+1, charge, module, hybrid, stream)]->GetXaxis()->SetRangeUser(0, 150);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->SetLineWidth(1);
						ht_extra[Form("gains_file%i_chip%i",file, chip_to_show)]=(TGraph*)data_file->Get(Form("%ipointscan_fit_data_gains", scan));

						//ht_extra[Form("gains_file%i_chip%i",file, chip_to_show)]->Print();
						Double_t *gain = ht_extra[Form("gains_file%i_chip%i",file, chip_to_show)]->GetY();
						//float gain = 7;
						float floaty_mega_boi = gain[hybrid*8+stream*4+chip_to_show];
						if(floaty_mega_boi==0.0){ floaty_mega_boi = 1;}
//
						printf("module %i hybrid %i stream %i chip %i (counter: %i) VAL: %f\n",module, hybrid, stream, chip_to_show, hybrid*18+stream*9+chip_to_show ,floaty_mega_boi );
		//				for(int i =0; i<5;i++){cout<<"HERE THE THING IS:              "<<gain[i] <<"and: "<< gain[hybrid*8+stream*4+chip_to_show]   << " for stream and module:"<< stream << " ," <<module <<endl;}
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Scale(1/floaty_mega_boi);
						//hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Scale(1/gain);
						hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      				legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("sigmas_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();


			}


		}}
		}


//SIGMAS- INPUT NOISE//////////////////////individual canvases//////////////////////////////////////////////////
	printf("Running the compare function - individual INPUT NOISE");
	printf("again again again 4\n");
	for(int module =0; module<modules; module++){
		for(int hybrid=0; hybrid<hybrids; hybrid++){
			if(hybrid==0 && chip_to_show==9){continue;}
		for(int stream =0; stream<2; stream++){
			//printf("scan %i size of data files %i\n", scan, data_files_size);
			for(int scan_val = 0; scan_val<scan; scan_val++){
				int charge=charges.at(scan_val);
				float charge_val=charge_vals.at(scan_val);
						canvas_map_in[Form("input_noise_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]=new TCanvas(Form("input_noise_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show),Form("input_noise_R%iH%i_stream%i_charge%i_chip%i", module, hybrid, stream, charge, chip_to_show), 1);
						legend_map[Form("input_noise_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)] = new TLegend(0.3,0.5,0.9,0.9);
						legend_map[Form("input_noise_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->SetHeader(Form("Input Noise charge %1.1f  R%iH%i Stream %i Chip %i", charge_val, module, hybrid, stream, chip_to_show ),"C");

					for(int file =0; file< data_files_size; file++){
						hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]=(TH1F*)hextra[Form("clone_file%i_charge%i_hist_sigma_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Clone();

					//	ht_extra[Form("gains_%i",file)]=(TGraph*)data_file->Get(Form("%ipointscan_fit_data_gains_allchips", scan));

						Double_t *gain = ht_extra[Form("gains_file%i_chip%i",file, chip_to_show)]->GetY();

						float floaty_mega_boi = gain[hybrid*8+stream*4+chip_to_show];
						if(floaty_mega_boi==0.0){floaty_mega_boi = 1;}
						hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Scale(1/floaty_mega_boi);
						hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_chip%i",file+1, charge, module, hybrid, stream, chip_to_show)]->Draw("SAME");
  		      				legend_map[Form("input_noise_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->AddEntry(hextra[Form("clone_file%i_charge%i_hist_input_noise_R%iH%i_stream%i_chip%i", file+1,charge, module, hybrid, stream, chip_to_show)],labels.at(file).c_str(),"l");
	      				}
					legend_map[Form("input_noise_R%iH%i_stream%i_charge%i_chip%i",module, hybrid, stream, charge, chip_to_show)]->Draw();

}
			}
		}
	}

}  //CompareChip





void HistStrobeComp::writeHist(){
	TDirectory *compare = fout->mkdir("compare");
	compare->cd();
	for ( auto itt:canvas_map){
               // std::cout<<"canvas_map: " << itt.first << std::endl;
		itt.second->Print(Form("pdfs/strobe/%s.pdf", itt.first.c_str()));
                itt.second->Write();

        }

	//////////////////////////////////////
	TDirectory *canvases = fout->mkdir("canvases");
	canvases->cd();
	for ( auto itt:canvas_map_in){
               // std::cout<<"canvas_map: " << itt.first << std::endl;

		itt.second->Print(Form("pdfs/strobe/%s.pdf", itt.first.c_str()));
                itt.second->Write();

			  }
	fout->cd();
///////////////////////////////////
	TDirectory *means = fout->mkdir("means");
	means->cd();
	for ( auto it:h){
                //std::cout<<"h: " << it.first << std::endl;
                it.second->Write();
        }
	//fout->cd();
	/////////////////////////
	TDirectory *sigmas = fout->mkdir("sigmas");
	sigmas->cd();
	for ( auto it:hsig){
        //       std::cout<<"hsig: " << it.first << std::endl;
                it.second->Write();
        }
//fout->cd();
				/////////////////////////////
				TDirectory *scan = fout->mkdir("scan");
				scan->cd();
				for ( auto it:h2){
			               // std::cout<<"h2: " << it.first << std::endl;
			                it.second->Write();
			        }
				for( auto itd:hd ){
			               // std::cout << "hd: "<< itd.first << std::endl;
			                itd.second->Write();
			        }
	fout->cd();
//	for ( auto it:ht){
        //       std::cout<<"hsig: " << it.first << std::endl;
  //              it.second->Write();
    //    }
	for ( auto it:canvas_map_h){
        //       std::cout<<"hsig: " << it.first << std::endl;
                it.second->Write();
                it.second->Print("pdfs/strobe/optimal_values.pdf");
        }

	fout->Close();
}

#include <iostream>
#include <stdio.h>
#include <TROOT.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TObject.h>
#include <TString.h>
#include "TH1F.h"
#include <fstream>
#include <map>
#include <string>

using namespace std;


int run(int argc, char* argv[]){
	printf("I am run \n");
	vector<string> file_names = vector<string>({
"strun131_1.root",
"strun131_2.root",
"strun131_3.root",
"strun131_4.root", 
"strun131_5.root",
"strun131_6.root",
"strun131_7.root",
"strun131_8.root",
"strun131_9.root",
"strun131_10.root",
"strun131_11.root",
"strun131_12.root",
"strun131_13.root",
"strun131_14.root",
"strun131_15.root",
"strun131_16.root",
"strun131_17.root",
"strun131_18.root",
"strun131_19.root",
"strun131_20.root",
"strun131_21.root",
"strun131_22.root",
"strun131_23.root",
"strun131_24.root",
"strun131_25.root",
//"strun131_26.root",
//"strun108_27.root",
//"strun108_28.root",
//"strun108_29.root",
//"strun108_30.root",
//"strun108_31.root",
//"strun108_32.root",
//"strun108_33.root",
//"strun108_34.root",
//"strun108_35.root",
//"strun108_36.root",
//"strun108_37.root",
//"strun108_38.root",
//"strun108_39.root",
//"strun108_40.root",
//"strun108_41.root",
//"strun108_42.root",
//"strun108_43.root",
//"strun108_44.root",
//"strun108_45.root",
//"strun108_46.root",
//"strun108_47.root",
//"strun108_48.root",
//"strun108_49.root",
//"strun108_50.root",
//"strun108_51.root",
//"strun108_52.root",
//"strun108_53.root",
//"strun108_54.root",
//"strun108_55.root",
//"strun108_56.root",
//"strun108_57.root",
//"strun108_58.root"


});


	
	for(int file=0; file<file_names.size();file++){
		string input_file = file_names.at(file);
		TString fName= TString(input_file.c_str());
		TFile *data_file=TFile::Open(fName);
		TObject *time = data_file->Get("Time");
		
		printf("input file: %s\ntime:", input_file.c_str());
		time->Print();
		printf("\n");

	}	



	return 0;
}


int main(int argc, char* argv[]){
	clock_t beginglobal = clock();

	run(argc, argv);

	clock_t endglobal = clock();
	double elapsed_secs = double(endglobal - beginglobal) / CLOCKS_PER_SEC;
	std::cout << "Time of computing: " << elapsed_secs << " = " << elapsed_secs/60 << " Minuten" <<std::endl;
	return 0;



}

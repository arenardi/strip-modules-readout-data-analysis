#include <iostream>
#include <stdio.h>
#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include <fstream>
#include <map>
#include <string>
#include "HistThreshComp.h"

using namespace std;

int RunNumber1, RunNumber2, RunNumber3 ;
int run(int argc, char* argv[]){

	printf("I am Run -> ");
	cin >> RunNumber1 >> RunNumber2 >> RunNumber3;

	int r_module = 0; //R0
	int modules=1;
	int hybrids = 2; //R0H0 and R0H1
	bool verbose = false;
	int channels = 256;
	int channel_to_show = -1;
	int chip_to_show = -1;
	bool allchips = false;
	int stream_to_show = 0; //stream 0 or 1
	int module_to_show = 0;
	int hybrid_to_show = 0; //module R0H 0 or 1
	bool tenpointscan = false;
	bool threepointscan = false;
	int scan  =0; //will become 3 or 10 when scan selected
	string out_file_name = "outThreshComp.root";

	for(int i_arg=1;i_arg<argc;i_arg++){
    		if(strcmp(argv[i_arg], "-chip")==0){chip_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-channel")==0){channel_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-module")==0){module_to_show=atoi(argv[i_arg+1]);i_arg++;}
				if(strcmp(argv[i_arg], "-hybrid")==0){hybrid_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-stream")==0){stream_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-v")==0){verbose=true;}
    		if(strcmp(argv[i_arg], "-allchips")==0){allchips=true;}
    		if(strcmp(argv[i_arg], "-3")==0){threepointscan=true;printf("3 point scan\n");scan=3;}
    		if(strcmp(argv[i_arg], "-10")==0){tenpointscan=true;printf("10 point scan\n");scan=10;}
        	if(strcmp(argv[i_arg], "-o")==0){out_file_name=string(argv[i_arg+1]);i_arg++;}
					//if(strcmp(argv[i_arg], "-run")==1007){RunNumber=atoi(argv[i_arg+1]);i_arg++;}
					//if(strcmp(argv[i_arg], "-scan")==2){ScanNumber=atoi(argv[i_arg+1]);i_arg++;}
	}

	printf("output stored in %s\n", out_file_name.c_str());
char file_name[30];
vector<string> file_names = vector<string>({});

	if(threepointscan || tenpointscan){
			sprintf(file_name,"out/Thresh%i.root",RunNumber1);
			file_names.push_back(file_name);
			sprintf(file_name,"out/Thresh%i.root",RunNumber2);
			file_names.push_back(file_name);
			sprintf(file_name,"out/Thresh%i.root",RunNumber3);
			file_names.push_back(file_name);
		}

	//vector<string> file_names = vector<string>({"out/Thresh434.root", "out/Thresh670.root", "out/Thresh704.root"});

	//limits of chips
	map<int,vector<int>> lims;

	if(r_module==0){

		lims[1]=vector<int>({0, 128});
		lims[2]=vector<int>({128, 256});
		lims[3]=vector<int>({256, 384});
		lims[4]=vector<int>({384, 512});

		lims[5]=vector<int>({768, 896});
		lims[6]=vector<int>({896, 1024});
		lims[7]=vector<int>({1024, 1152});
		lims[8]=vector<int>({1152, 1280});
		lims[9]=vector<int>({1280, 1408});
	}

	if(chip_to_show ==0 || chip_to_show > 9){printf("Error: Please enter a chip out of 1-> 9 and select Stream 0 or 1 and Module R0H 0 or 1. Channel can be provided within the range (1, 256).\n");return 0;}
	if(channel_to_show == 0 || channel_to_show > 256){printf("Error: Please enter a chip out of 1-> 9 and select Stream 0 or 1 and Module R0H 0 or 1. Channel can be provided within the range (1, 256).\n");return 0;}
	if(chip_to_show>0 && channel_to_show>0){
		channel_to_show = lims[chip_to_show].at(0)+channel_to_show;
		chip_to_show = -1;
	}
	vector<string> labels = vector<string>({"Hybrid Run 434", "Module LV Run 670", "Module HV Run 704"});
	vector<TFile*> data_files = vector<TFile*>({});
	printf("Input files + labels:\n");
	for(int file = 0; file < file_names.size(); file++){
		string input_file = file_names.at(file);
		cout<<"\t"<<input_file<<endl;
		cout<<"\t\t"<<labels.at(file)<<endl;
		TString fName= TString(input_file.c_str());
		TFile *data_file=TFile::Open(fName);
		data_files.push_back(data_file);
	}
//printf("1\n");
	HistThreshComp *hist = new HistThreshComp(out_file_name,lims, scan);
//	printf("2\n");

	hist->make(data_files, modules, hybrids, verbose);
//	printf("2\n");
//cout<<"1000"<<endl;

	if(channel_to_show>0 || chip_to_show>0){
		std::cout << "S Curve requested"<< std::endl;
		hist->GetSCurve(module_to_show, hybrid_to_show, stream_to_show, channel_to_show, chip_to_show);
	}
	if(scan==3||scan==10){
		std::cout << "Scan " << scan << " requested" << std::endl;
		hist->Compare(scan, labels, data_files, modules, hybrids, chip_to_show, allchips);
	}
	hist->writeHist();
}





int main(int argc, char* argv[]){
	clock_t beginglobal = clock();

	run(argc, argv);

	clock_t endglobal = clock();
	double elapsed_secs = double(endglobal - beginglobal) / CLOCKS_PER_SEC;
	std::cout << "Time of computing: " << elapsed_secs << " Sekunden  = " << elapsed_secs/60 << " Minuten" <<std::endl;
	return 0;



}

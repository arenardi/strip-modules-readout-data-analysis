#include <iostream>
#include <stdio.h>
#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include <fstream>
#include <map>
#include <string>
#include "HistStrobe.h"

using namespace std;

int RunNumber, ScanNumber;
int run(int argc, char* argv[]){
	printf("I am Run Scan: \n");
	cin >> RunNumber >> ScanNumber;


	int modules = 1; //R0
	int module=2;
//if (module==3) {	int hybrids=4;}
	int chips=7;
	int hybrids = 2; //R0H0 and R0H1
	bool verbose = false;
	int channels = 256;
	int channel_to_show = -1;
	int chip_to_show = -1;
	int stream_to_show = 0; //stream 0 or 1
	int hybrid_to_show = 0; //module R0H 0 or 1
	int scan  =0; //will become 3 or 10 when scan selected
	bool allchips = false;
	char out_file_name[30];
	sprintf(out_file_name,"out/Strobe%i.root",RunNumber);


	int option = 1;
	for(int i_arg=1;i_arg<argc;i_arg++){
    		if(strcmp(argv[i_arg], "-chip")==0){chip_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-channel")==0){channel_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-hybrid")==0){hybrid_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-stream")==0){stream_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-option")==0){option=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-v")==0){verbose=true;}
    		if(strcmp(argv[i_arg], "-allchips")==0){allchips=true;}
        	//if(strcmp(argv[i_arg], "-o")==0){out_file_name=string(argv[i_arg+1]);i_arg++;}
					if(strcmp(argv[i_arg], "-run")==1007){RunNumber=atoi(argv[i_arg+1]);i_arg++;}
					if(strcmp(argv[i_arg], "-scan")==2){ScanNumber=atoi(argv[i_arg+1]);i_arg++;}
	}
	char file_name[30];
	sprintf(file_name,"strun%i_%i.root",RunNumber,ScanNumber);
	vector<string> file_names = vector<string>({string(file_name)});

cout<<"input run: "<<file_name<<endl;
cout<<"output in: "<<out_file_name<<endl;
		//if(option==0){out_file_name = "outs/strobe108.root"; file_names = vector<string>({"data/strun108_28.root"});}
		//if(option==1){out_file_name = "outs/strobe131.root"; file_names = vector<string>({"data/strun131_2.root"});}

	//printf("output stored in %s\n", out_file_name.c_str());
	map<int,vector<int>> lims;
	lims[1]=vector<int>({1, 128});
	lims[2]=vector<int>({128, 256});
	lims[3]=vector<int>({256, 384});
	lims[4]=vector<int>({384, 512});
	lims[5]=vector<int>({512,640});
	lims[6]=vector<int>({640,768});


	if(chip_to_show ==0 || chip_to_show > 9){printf("Error: Please enter a chip out of 1-> 9 and select Stream 0 or 1 and Module R0H 0 or 1. Channel can be provided within the range (1, 256).\n");return 0;}
	if(channel_to_show == 0 || channel_to_show > 256){printf("Error: Please enter a chip out of 1-> 9 and select Stream 0 or 1 and Module R0H 0 or 1. Channel can be provided within the range (1, 256).\n");return 0;}
	if(chip_to_show>0 && channel_to_show>0){
		channel_to_show = lims[chip_to_show].at(0)+channel_to_show;
		chip_to_show = -1;
	}

	vector<TFile*> files = vector<TFile*>({});
	for(int file = 0; file < file_names.size(); file++){
		string input_file = file_names.at(file);
		cout<<input_file<<endl;
		TString fName= TString(input_file.c_str());
		TFile *data_file=TFile::Open(fName);
		files.push_back(data_file);
	}

	HistStrobe *hist = new HistStrobe(out_file_name,lims);

	hist->make(files, module, hybrids, chips, verbose);
	//hist->Compare();
	//hist->stupidPrint();
//	if(channel_to_show>0 || chip_to_show>0){
//		std::cout << "S Curve requested"<< std::endl;
//		hist->GetSCurve(module_to_show, stream_to_show, channel_to_show, chip_to_show);
//	}
	hist->writeHist();
}





int main(int argc, char* argv[]){
	clock_t beginglobal = clock();

	run(argc, argv);

	clock_t endglobal = clock();
	double elapsed_secs = double(endglobal - beginglobal) / CLOCKS_PER_SEC;
	std::cout << "Time of computing: " << elapsed_secs << " Sekunden  = " << elapsed_secs/60 << " Minuten" <<std::endl;
	return 0;



}

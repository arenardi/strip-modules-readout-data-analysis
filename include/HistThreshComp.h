//HistHold.h
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include "TF1.h"
#include <iostream>
#include <vector>
#include <map>
#include <sstream>
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#ifndef Histhold_H
#define Histhold_H

using namespace std;

class HistThreshComp{
    public:
        HistThreshComp(string out_file_name, map< int,vector<int>> lims, int scany){fout=new TFile(out_file_name.c_str(),"RECREATE"); limits = lims; scan = scany;}
        int make(vector<TFile*> input, int modules, int hybrids, bool v);
        void GetSCurve(int module, int hybrid, int stream, int channel, int chip);
	void writeHist();
	void Compare(int n, vector<string> labels, vector<TFile*> data_files, int modules, int hybrids, int chip_to_show, bool allchips);
	void CompareChip(int n, vector<string> labels, vector<TFile*> data_files, int chip_to_show);
	void stupidPrint() { 
		for (const auto& p : fits) {
			std::cout << p.first << ", " << p.second << std::endl;
		}
	}

    private:
        map<string,TH1F*> h;
        map<string,TH1F*> hsig;
        map<string, TCanvas*> canvas_map; 
        map<string, TCanvas*> canvas_map_in; 
        map<string, TLegend*> legend_map; 
	//vector<string> labels;	
 
	map<string,TH2F*> h2;
        map<string,TH1F*> hextra;
        map<string,TGraph*> ht;
        map<string,TGraph*> ht_extra;
        map<string,float> counter;
        vector<float> charge_vals;
        vector<int> charges;
        map<string,TH1D*> hd;
	vector<TFile*> data_files;
       	map<int, vector<int>> limits; 
        map<string,TF1*> fits;
	bool verbose;
	int modules;
	int hybrids;
	int data_files_size;
	int scan;
	TFile *fout;
};
#endif



//HistHold.h
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include "TF1.h"
#include <iostream>
#include <vector>
#include <map>
#include <sstream>
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#ifndef Histstrobe_H
#define Histstrobe_H

using namespace std;

class HistStrobe{
    public:
        HistStrobe(string out_file_name, map< int,vector<int>> lims){fout=new TFile(out_file_name.c_str(),"RECREATE"); limits = lims;}
        int make(vector<TFile*> input, int module, int hybrids, int chips, bool v);
        void GetSCurve(int module,int hybrid, int stream, int channel, int chip);
	void writeHist();
	void stupidPrint() {
		for (const auto& p : fits) {
			std::cout << p.first << ", " << p.second << std::endl;
		}
	}

    private:
        map<string,TH1F*> h;
        map<string,TH1F*> hsig;
        map<string,TH1F*> her;
        map<string,TH2F*> h2;
        map<string,TH1F*> hextra;
        map<string,TGraph*> ht;
        map<string,TGraph*> htfit;
        map<string,float> counter;
        map<string,float> first;
        map<string,float> last;
        map<string,float> optimal;
	
        map<string,TH1D*> hd;
	vector<TFile*> data_files;
       	map<int, vector<int>> limits; 
        map<string,TF1*> fits;
	bool verbose;
	int hybrids;
	int modules;
	int module;
	int chips;
	TFile *fout;
};
#endif



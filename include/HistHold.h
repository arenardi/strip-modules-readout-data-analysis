//HistHold.h
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include "TF1.h"
#include <iostream>
#include <vector>
#include <map>
#include <sstream>
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#ifndef Histhold_H
#define Histhold_H

using namespace std;

class HistHold{
    public:
        HistHold(string out_file_name, map< int,vector<int>> lims, int scany){fout=new TFile(out_file_name.c_str(),"RECREATE"); limits = lims; scan = scany;}
        int make(vector<TFile*> input, int modules, bool v);
        void GetSCurve(int module, int stream, int channel, int chip);
	void writeHist();
	void Scan(int n);
	void stupidPrint() {
		for (const auto& p : fits) {
			std::cout << p.first << ", " << p.second << std::endl;
		}
	}

    private:
        map<string,TH1F*> h;
        map<string,TH2F*> h2;
        map<string,TH1F*> hextra;
        map<string,TGraph*> ht;
        map<string,float> counter;
        vector<float> charge_vals;
        //vector<float> charges;
        vector<int> charges;
        map<string,TH1D*> hd;
	vector<TFile*> data_files;
       	map<int, vector<int>> limits; 
        map<string,TF1*> fits;
	bool verbose;
	int modules;
	int scan;
	TFile *fout;
};
#endif



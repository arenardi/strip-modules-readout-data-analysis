#include <iostream>
#include <stdio.h>
#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "TH1F.h"
#include "TH2F.h"
#include "TH1D.h"
#include <fstream>
#include <map>
#include <string>
#include "HistHold.h" 

using namespace std;


int run(int argc, char* argv[]){
	printf("I am run \n");
	int r_module = 0; //R0
	int modules = 2; //R0H0 and R0H1
	bool verbose = false;
	int channels = 256;
	int channel_to_show = -1;
	int chip_to_show = -1;
	int stream_to_show = 0; //stream 0 or 1
	int module_to_show = 0; //module R0H 0 or 1
	bool tenpointscan = false;
	bool threepointscan = false;
	int scan  =0; //will become 3 or 10 when scan selected
	string out_file_name = "out.root";

	for(int i_arg=1;i_arg<argc;i_arg++){
    		if(strcmp(argv[i_arg], "-chip")==0){chip_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-channel")==0){channel_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-module")==0){module_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-stream")==0){stream_to_show=atoi(argv[i_arg+1]);i_arg++;}
    		if(strcmp(argv[i_arg], "-v")==0){verbose=true;}
    		if(strcmp(argv[i_arg], "-3")==0){threepointscan=true;printf("3 point scan\n");scan=3;}
    		if(strcmp(argv[i_arg], "-10")==0){tenpointscan=true;printf("10 point scan\n");scan=10;}
        	if(strcmp(argv[i_arg], "-o")==0){out_file_name=string(argv[i_arg+1]);i_arg++;}

	}

	printf("output stored in %s\n", out_file_name.c_str());
	vector<string> file_names = vector<string>({"threepointscan/strun108_3.root"});
	//if(threepointscan){file_names = vector<string>({"strun108_3.root","strun108_4.root", "strun108_5.root"});}
	if(threepointscan){
		file_names = vector<string>({"threepointscan/strun94_3.root","threepointscan/strun94_4.root", "threepointscan/strun94_5.root"});
	}
	if(tenpointscan){
		file_names = vector<string>({"tenpointscan/strun108_49.root","tenpointscan/strun108_50.root", "tenpointscan/strun108_51.root", "tenpointscan/strun108_52.root", "tenpointscan/strun108_53.root", "tenpointscan/strun108_54.root", "tenpointscan/strun108_55.root", "tenpointscan/strun108_56.root", "tenpointscan/strun108_57.root", "tenpointscan/strun108_58.root"});
	}
	
	//limits of chips
	map<int,vector<int>> lims;
	if(r_module==0){
		lims[1]=vector<int>({0, 256});
		lims[2]=vector<int>({256, 512});
		lims[3]=vector<int>({768, 1024});
		lims[4]=vector<int>({1025, 1280});
	}


	if(chip_to_show ==0 || chip_to_show > 4){printf("Error: Please enter a chip out of [1,2,3,4] and select Stream 0 or 1 and Module R0H 0 or 1. Channel can be provided within the range (1, 256).\n");return 0;}
	if(channel_to_show == 0 || channel_to_show > 256){printf("Error: Please enter a chip out of [1,2,3,4] and select Stream 0 or 1 and Module R0H 0 or 1. Channel can be provided within the range (1, 256).\n");return 0;}
	if(chip_to_show>0 && channel_to_show>0){
		channel_to_show = lims[chip_to_show].at(0)+channel_to_show;
		chip_to_show = -1;
	}

	vector<TFile*> files = vector<TFile*>({});
	for(int file = 0; file < file_names.size(); file++){	
		string input_file = file_names.at(file);
		cout<<input_file<<endl;
		TString fName= TString(input_file.c_str());
		TFile *data_file=TFile::Open(fName);
		files.push_back(data_file);
	}
	
	HistHold *hist = new HistHold(out_file_name,lims, scan);
	hist->make(files, modules, verbose);
	//hist->stupidPrint();
	if(channel_to_show>0 || chip_to_show>0){
		std::cout << "S Curve requested"<< std::endl;
		hist->GetSCurve(module_to_show, stream_to_show, channel_to_show, chip_to_show);
	}
	if(scan==3||scan==10){
		std::cout << "Scan " << scan << " requested" << std::endl;
		hist->Scan(scan);
	}
	hist->writeHist();	
}





int main(int argc, char* argv[]){
	clock_t beginglobal = clock();

	run(argc, argv);

	clock_t endglobal = clock();
	double elapsed_secs = double(endglobal - beginglobal) / CLOCKS_PER_SEC;
	std::cout << "Time of computing: " << elapsed_secs << " Sekunden  = " << elapsed_secs/60 << " Minuten" <<std::endl;
	return 0;



}
